#! /usr/bin/env python
'''
Read the FID from any ROOT/POOL file

ROOT files are assumed to have the key 'Refs' and have less than 10 keys
POOL files are assumed to have the keys '\#\#Shapes', '\#\#Links', '\#\#Params'

useage: ReadGUID.py [--verbose, -V, --debug, -D] [--help, -H] <filename>

option: --verbose, -V, --debug, -D, print more verbose information
        --help, -H, print this help and exit
'''
from __future__ import print_function
import sys
filename=''
isVerbose=False
year="2012"

def usage():
    print(__doc__)

def parseArgs():
    global isVerbose
    global filename
    for arg in sys.argv[1:]:
        if arg.lower() in ['--verbose', '-v', '--debug', '-d']:
            isVerbose=True
        elif arg.lower() in ['--help', '-h']:
            usage()
            sys.exit(0)
        elif filename!='':
            usage()
            raise ValueError('one file at a time please')
        else:
            filename=arg
    if filename=='':
        usage()
        raise ValueError('please supply a filename')

# a simple class with a write method
class WritableObject:
    def __init__(self):
        self.content = []
    def write(self, string):
        self.content.append(string)


try:
    import ROOT
except:
    usage()
    raise ImportError('ROOT modules do not exist, call this script from within an Environment with ROOT')

parseArgs()

def printOFF():
    sys.stdout=WritableObject()
    sys.stderr=WritableObject()

def printON():
    sys.stdout=sys.__stdout__
    sys.stderr=sys.__stderr__
    
    

if not isVerbose:
    printOFF()

thefile=ROOT.TFile.Open(filename)

if not isVerbose:
    printON()

def atEnd():
    global thefile
    if thefile:
        thefile.Close()

if not thefile or (hasattr(thefile, "isZombie") and thefile.isZombie) or (hasattr(thefile,"IsZombie") and thefile.IsZombie()):
    atEnd()
    raise IOError("the file could not be opened by Root")

keynames=[key.GetName() for key in thefile.GetListOfKeys()]

if isVerbose:
    print(keynames)

#three keys must be there in a pool file
poolkeys=['##Shapes', '##Links', '##Params']

isPool=(len([akey for akey in keynames if akey in poolkeys])==len(poolkeys))

#one key must be there in a Root file
rootkeys=['Refs']

isRoot=(len(keynames)<10 and len([akey for akey in keynames if akey in rootkeys])==len(rootkeys))

if isRoot==isPool:
    print("Error: UNKNOWN file type. GUID can only be obtained from a ROOT-like file")
    atEnd()
    sys.exit(1)


if isVerbose:
    print("Success: "+persistency+" type detected")

def fromPoolFile(f):
    t=f.Get("##Params")
    #if isVerbose:
    #if not isVerbose:
    #    printOFF()
    t.GetEvent(0)
    if isVerbose:
        t.Show(0)
    #if not isVerbose:
    #    printON()
    leaves=t.GetListOfLeaves()
    leaf=leaves.UncheckedAt(0)
    val=leaf.GetValueString()
    if isVerbose:
        print(val)
    return val

def fromRootFile(f):
    import ctypes
    t=f.Get("Refs")
    astring=ctypes.create_string_buffer(256)
    b=t.GetBranch("Params")
    b.SetAddress(astring)
    for i in range(b.GetEntries()):
        b.GetEvent(i)
        if isVerbose:
            print(astring)
            print(astring[:])
        if astring[0:4]=="FID=":
            return ""+astring[:]
    return ""

try:
    val=None
    if isRoot:
        val=fromRootFile(thefile)
    else:
        val=fromPoolFile(thefile)

    print(val.split("=")[-1].split("]")[0])
except:
    atEnd()
    raise

atEnd()

sys.exit(0)
