#! /usr/bin/env python
'''
Detect whether a file was written in POOL or ROOT format.
This only needs the list of Keys, and an environment containing ROOT

ROOT files are assumed to have the key 'Refs' and have less than 10 keys
POOL files are assumed to have the keys '\#\#Shapes', '\#\#Links', '\#\#Params'
'''
from __future__ import print_function
import sys
filename=''
isVerbose=False

def usage():
    print('''
Detect whether a file was written in POOL or ROOT format.
This only needs the list of Keys, and an environment containing ROOT

ROOT files are assumed to have the key 'Refs' and have less than 10 keys
POOL files are assumed to have the keys '\#\#Shapes', '\#\#Links', '\#\#Params'

useage: '''+sys.argv[0]+''' [--verbose, -V, --debug, -D] [--help, -H] <filename>

option: --verbose, -V, --debug, -D, print more verbose information
        --help, -H, print this help and exit
    ''')

def parseArgs():
    global isVerbose
    global filename
    for arg in sys.argv[1:]:
        if arg.lower() in ['--verbose', '-v', '--debug', '-d']:
            isVerbose=True
        elif arg.lower() in ['--help', '-h']:
            usage()
            sys.exit(0)
        elif filename!='':
            usage()
            raise ValueError('one file at a time please')
        else:
            filename=arg
    if filename=='':
        usage()
        raise ValueError('please supply a filename')

# a simple class with a write method
class WritableObject:
    def __init__(self):
        self.content = []
    def write(self, string):
        self.content.append(string)

try:
    import ROOT
except:
    usage()
    raise ImportError('ROOT modules do not exist, call this script from within an Environment with ROOT')

parseArgs()

if not isVerbose:
    sys.stdout=WritableObject()
    sys.stderr=WritableObject()

thefile=ROOT.TFile.Open(filename)

if not isVerbose:
    sys.stdout=sys.__stdout__
    sys.stderr=sys.__stderr__

if not thefile or thefile.isZombie:
    raise IOError("the file could not be opened by Root")

keynames=[key.GetName() for key in thefile.GetListOfKeys()]

if isVerbose:
    print(keynames)

#three keys must be there in a pool file
poolkeys=['##Shapes', '##Links', '##Params']

isPool=(len([akey for akey in keynames if akey in poolkeys])==len(poolkeys))

#one key must be there in a Root file
rootkeys=['Refs']

isRoot=(len(keynames)<10 and len([akey for akey in keynames if akey in rootkeys])==len(rootkeys))

if isRoot==isPool:
    print("UNKNOWN")
elif isRoot:
    print("ROOT")
elif isPool:
    print("POOL")

if thefile:
    thefile.Close()
