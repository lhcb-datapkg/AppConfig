2018-04-30 AppConfig v3r358
===

- Add options files for 2018 Tesla processing, !14 (@apearce)  
  Needed for 2018 Tesla productions.

- Add options for upgrade MC filtering with ZombieMoore, !15 (@olupton)  
  Also, added `Conditions/TCK-x52000000.py`, corresponding to the planned first TCK in the upgrade series.

- Specify input raw event format for 2017 Tesla MC, !16 (@apearce)  
  Updates options/Turbo/Tesla_Simulation_2017.py to specify correct `SplitRawEventInput` format
  