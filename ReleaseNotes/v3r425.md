2023-12-18 AppConfig v3r425
===

This version is released on `v3-patches` branch.
Built relative to AppConfig [v3r424](../-/tags/v3r424), with the following changes:

### New features ~"new feature"

- Beamfiles for expected-2024 pp with nu-1.6, 4.3, 5.7, !223 (@kreps)

