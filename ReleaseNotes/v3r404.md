2021-04-13 AppConfig v3r404
===

This version is released on `master` branch.
Built relative to AppConfig [v3r403](../-/tags/v3r403), with the following changes:

### New features ~"new feature"

- Appconfig for s29r2p2, !144 (@fredi)

### Fixes ~"bug fix" ~workaround

- Remove raw banks for StrippingHlt2PassThroughLine for S24r1, S28r1, S28r2 and S29r2 MC, !143 (@atully)
