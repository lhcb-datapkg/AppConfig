2024-03-21 AppConfig v3r427
===

This version is released on `v3-patches` branch.
Built relative to AppConfig [v3r426](../-/tags/v3r426), with the following changes:

### New features ~"new feature"

- Disable spillover for VP in Boole for Run3, !231 (@gcorti)
 