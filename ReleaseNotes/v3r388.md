

2019-09-10 AppConfig v3r388
========================================

This version is released on `master` branch.

### Simulation

- GaussMP patch, !92 (@valassi) [LHCBGAUSS-1409,LHCBGAUSS-1779,LHCBGAUSS-1780,LHCBGAUSS-1781,LHCBGAUSS-1785,LHCBGAUSS-1786]  
  Comprehensive GaussMP patch with workarounds for LHCBGAUSS-1409, LHCBGAUSS-1779, LHCBGAUSS-1780, LHCBGAUSS-1785, LHCBGAUSS-1786.  
