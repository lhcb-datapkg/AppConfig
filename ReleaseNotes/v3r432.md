2024-06-20 AppConfig v3r432
===

This version is released on `v3-patches` branch.
Built relative to AppConfig [v3r431](../-/tags/v3r431), with the following changes:

### Enhancements ~enhancement

- Add options file for magnetic field map v6r1, !248 (@admorris)
- Add file Beam6800GeV-moff-2024.Q1.2-nu{4.3,5.7,7.6}.py for 2024 Magnet Off production, !247 (@admorris)
- Add option for 2024 reprocessing of 2016 pHe data, !244 (@chlucare)

