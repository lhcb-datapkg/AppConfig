2019-03-15 AppConfig v3r379
========================================

This version is released on `master` branch.

### New features

- Added options file for redecaying the heaviest ancestor of the signal, !63 (@dmuller)   


### Bug fixes

- Options to use the Sim09 tuning for Pythia8, !68 (@dmuller)   
  Needed for upgrade MC productions of Bs event types.

  
