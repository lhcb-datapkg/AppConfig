## ############################################################################
## # File for running Gauss with Baseline Upgrade detectors and Low Energy
## # configuration for Muon Background Parameterization
## #
## # p.griffith 2019-10-18
## ############################################################################

importOptions('$GAUSSOPTS/Infrastructure.py')
#TODO: import (e.g) "FTFP_BERT_HP_NoCherenkov" option 

## Physics Setup
# - FTFP-BERT with HP hadronic physics list
# - RICH Cherenkov OFF 
# - One fixed interaction
# - Production cuts applied
# - Tracking cuts applied
# - Muon filter physics thresholds overridden
##

importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
importOptions('$GAUSSOPTS/MuonLowEnergy.py')
