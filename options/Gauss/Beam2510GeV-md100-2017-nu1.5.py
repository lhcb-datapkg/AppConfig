#   Beam 2.51 TeV, beta* = 3.1m , emittance(normalized) ~ 2.5 micron
#   External Horizontal Crossing Angle
#   No spill-over
#   To switch on the spill-over a dedicate file has to be added
#   Magnet down
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam2510GeV-md100-2017-nu1.5.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
Gauss().Luminosity        = 0.275*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 87*SystemOfUnits.millibarn

#--Set the average position of the IP:from Federico Alessio, LHCBGAUSS-2462
Gauss().InteractionPosition = [  0.891*SystemOfUnits.mm,
                                -0.218*SystemOfUnits.mm,
                                -7.500*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 50.93*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 2.51*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.630*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0329 mm ~ 33 um
Gauss().BeamEmittance     = 0.0026*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.1*SystemOfUnits.m
