#   Copy from Beam2680GeV-md100-expected-2024-nu3.5.py, change nu value only.
#
#   Beam 2.68 TeV, beta* = 3.1m , emittance(normalized) ~ 2.5 micron
#   External Horizontal Crossing Angle
#   No spill-over
#   To switch on the spill-over a dedicate file has to be added
#   Magnet down
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam2680GeV-md100-expected-2024-nu5.1.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
# 
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
# Keep same cross-section as with 2.51 TeV beam momentum
# Corresponds to nu=2.9
Gauss().Luminosity        = 0.3748*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 87*SystemOfUnits.millibarn

#--Set the average position of the beam to be same as in expected-2024 pp at full energy
Gauss().InteractionPosition = [ 0.440*SystemOfUnits.mm,
                                0.023*SystemOfUnits.mm,
                                -2.450*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 50.93*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 2.68*SystemOfUnits.TeV
Gauss().B2Momentum        = -2.68*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.352*SystemOfUnits.mrad  # internal
Gauss().BeamVCrossingAngle =  0.170*SystemOfUnits.mrad  # external
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3.1m and e_norm 2.6um)
# Gives \sigma_{x,y} = sqrt(beta* x emittance / 2) = 0.0386 mm ~ 38 um
Gauss().BeamEmittance     = 0.0026*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.1*SystemOfUnits.m
