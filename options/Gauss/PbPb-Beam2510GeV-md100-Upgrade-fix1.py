# This is copy of PbPb-Beam2510GeV-md100-2018-fix1.py which was used before
# for Upgrade PbPb production, just setting interaction position to 0.

from Gauss.Configuration import *

#--Set the energy of the beams,
Gauss().BeamMomentum = 2510*SystemOfUnits.GeV
Gauss().B2Momentum = -2510*SystemOfUnits.GeV
Gauss().B1Particle = 'Pb'
Gauss().B2Particle = 'Pb'

#--Not used since OneFixedInteraction.py is called
Gauss().Luminosity        = (10**26)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 8600*SystemOfUnits.millibarn

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [  0.0*SystemOfUnits.mm,
                                 0.0*SystemOfUnits.mm,
                                 0.0*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
# en gros bunchRMS double de sigmaZ?
Gauss().BunchRMS = 58.8*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.285*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle = -0.050*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0329 mm ~ 33 um
Gauss().BeamEmittance     = 0.0023*SystemOfUnits.mm #(0.0034 en 2015)
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
