from Configurables import Generation
from Configurables import FlatZSmearVertex
#reduce the Z production zone to [-210,-90]mm
for algName in ["Generation", "BeamGasGeneration","BeamElectronGeneration"]:
    generation=Generation(algName)
    generation.VertexSmearingTool = "FlatZSmearVertex"
    generation.addTool( FlatZSmearVertex ) 
    generation.FlatZSmearVertex.ZMin = -210. 
    generation.FlatZSmearVertex.ZMax = -90.
