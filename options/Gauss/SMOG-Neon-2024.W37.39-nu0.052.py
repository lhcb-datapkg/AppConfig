# File for setting SMOG injection of Neon during 2024.W37.39 - Block6.
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-Ne)=3.657933 x 10^30 cm-2 s-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb = 1.5241 x 10^27 cm-2 s-1,
#  Correspnding to nu(p-Ne)=0.0524, and mu(p-Ne)=0.0366.

Gauss().FixedTargetParticle = 'Ne'
Gauss().FixedTargetLuminosity = 1.52*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 386.68*SystemOfUnits.millibarn
