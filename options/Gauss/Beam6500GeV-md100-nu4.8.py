# File for setting hypotetical Beam conditions
# They are suitable for post-LS1 studies:
#   Beam 6.5 TeV, beta* = 3m , emittance(normalized) ~ 2.5 micron
#   External Horizontal Crossing Angle
#   No spill-over
#   To switch on the spill-over a dedicate file has to be added
#   Magnet down
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-4.8.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch, corresponding to
#                           L = 7 x 10^32 cm-2 s-1 with 1324 colliding bunches
#                           (1324 num. of colliding bunches with 50 ns in Run I)
#  It correspond to nu(total) = 4.77
Gauss().Luminosity        = 0.529*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 101.5*SystemOfUnits.millibarn

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [ 0.0, 0.0, 0.0 ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 90.0*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 6.5*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.395*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.0025*SystemOfUnits.mm
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0329 mm ~ 33 um
Gauss().BeamBetaStar      = 3.00*SystemOfUnits.m
