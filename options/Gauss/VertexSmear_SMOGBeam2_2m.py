from Configurables import Generation
from Configurables import FlatZSmearVertex 
for algName in ["Generation", "GasBeamGeneration"]:
    generation=Generation(algName)
    generation.VertexSmearingTool = "FlatZSmearVertex"
    generation.addTool( FlatZSmearVertex ) 
    generation.FlatZSmearVertex.ZMin = -600
    generation.FlatZSmearVertex.ZMax = 1400.
