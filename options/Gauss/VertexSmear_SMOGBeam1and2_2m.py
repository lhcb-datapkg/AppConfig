from Configurables import Generation
from Configurables import FlatZSmearVertex 

for algName in ["BeamGasGeneration", "GasBeamGeneration", "BeamElectronGeneration"]:
    generation=Generation(algName)
    generation.VertexSmearingTool = "FlatZSmearVertex"
    generation.addTool( FlatZSmearVertex )
    generation.FlatZSmearVertex.ZMin = -1400.
    generation.FlatZSmearVertex.ZMax =  600.


