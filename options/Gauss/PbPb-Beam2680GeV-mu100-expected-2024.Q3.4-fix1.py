# File for setting hypotetical PbPb Beam conditions for post-TS1 2024
# Fixed InteractionPosition to be the same as pp ref run 2024

from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the type of beams
Gauss().B1Particle        = 'Pb'
Gauss().B2Particle        = 'Pb'

#--Set the energy of the beams
Gauss().BeamMomentum      = 2680*SystemOfUnits.GeV
Gauss().B2Momentum        = -2680*SystemOfUnits.GeV

#  Expected luminosity leveling in IP8 = 1.5 × 10^27 cm−2 s−1 
#  Instantaneous luminosity per bunch crossing with 1240 colling bunches = 0.121 x 10^25 cm-2 s-1
#  Pile-up should be around 9.25 x 10^-4, much below one, thus fix one interaction
Gauss().Luminosity        = 0.121*(10**25)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 8600*SystemOfUnits.millibarn

#--Set the average position of the IP:
# use post-VELO intervention, see https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/merge_requests/261
Gauss().InteractionPosition = [ 0.440*SystemOfUnits.mm,
                                0.023*SystemOfUnits.mm,
                               3.016*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region, Bunch RMS = sqrt(2)*PV_Z_RMS
#  Nominally it is 90mm (1.2 ns) but it is reduced due to Geometrical Factor
#  SigmaX and SigmaY are calculated from the beta* and emittance
# Keep the sigmaZ of the Pb beams from measured 2023 PbPb data
Gauss().BunchRMS = 66.31*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
# Angles taken from https://lpc.web.cern.ch/Run3/HIConfiguration2024.html: 
# external crossing angle for pp reference and PbPb will be fully horizontal
Gauss().BeamHCrossingAngle = ((+0.139)+(-0.235))*SystemOfUnits.mrad # internal and external
Gauss().BeamVCrossingAngle = 0*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ] #ideal conditions: to be adjusted with real data

#--beta* and emittance (beta* is nomimally 2m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Parameters taken from https://lpc.web.cern.ch/Run3/HIConfiguration2024.html
Gauss().BeamEmittance     = 0.00165*SystemOfUnits.mm
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

#One fixedinteraction due to pile-up below 1
from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

