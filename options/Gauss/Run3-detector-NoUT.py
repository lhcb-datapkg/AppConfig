############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
# the actual version is goign to be picked up by selecting the dbase tags
# In addition this file includes the geometry of the SciFi neutron Shield
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['VP', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }
Gauss().DetectorMoni = { "Detectors": ['VP', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet' ] }

