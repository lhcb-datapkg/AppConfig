# File for setting Beam conditions for post-TS1 2024.W31.34
#
# Syntax is:
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam6800GeV-mu100-2024.W31.34-nu6.3.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Tags-YEAR.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the type of beams
Gauss().B1Particle        = 'p'
Gauss().B2Particle        = 'p'

#--Set the energy of the beams
Gauss().BeamMomentum      = 6.8*SystemOfUnits.TeV
Gauss().B2Momentum        = -6.8*SystemOfUnits.TeV

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch, corresponding to
#                           L = 2 x 10^33 cm-2 s-1 with 2400 colliding bunches
#  It correspond to nu(total) = 6.3 and we assume mu(visible)=0.699*nu
Gauss().Luminosity        = 0.6922*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn

#--Set the average position of the IP:
# use post-VELO intervention fills 9905-9927
Gauss().InteractionPosition = [ 0.440*SystemOfUnits.mm,
                                0.023*SystemOfUnits.mm,
                                3.016*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region, Bunch RMS = sqrt(2)*PV_Z_RMS
#  Nominally it is 90mm (1.2 ns) but it is reduced due to Geometrical Factor
#  SigmaX and SigmaY are calculated from the beta* and emittance
Gauss().BunchRMS = 62.33*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
Gauss().BeamHCrossingAngle = +0.138*SystemOfUnits.mrad # internal
Gauss().BeamVCrossingAngle = +0.200*SystemOfUnits.mrad # external
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 2m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.005285*SystemOfUnits.mm
# Gives \sigma_{x,y}(beam) = 0.0456 mm ~= \sigma_{x,y}(PV) = 0.0322 mm
Gauss().BeamBetaStar      = 2.0*SystemOfUnits.m
