from Configurables import Gauss
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions

Gauss().BeamMomentum = 6500*SystemOfUnits.GeV
Gauss().B2Momentum = 0*SystemOfUnits.GeV
Gauss().B1Particle = 'p'
Gauss().B2Particle = 'Ne'

#--Not used since OneFixedInteraction.py is called
Gauss().Luminosity        = 0.177*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 950*SystemOfUnits.millibarn

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [  0.738*SystemOfUnits.mm,
                                -0.187*SystemOfUnits.mm,
                                     0*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 53.60*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.502*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle = -0.056*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0329 mm ~ 33 um
Gauss().BeamEmittance     = 0.0005*SystemOfUnits.mm
Gauss().BeamBetaStar      = 24.0*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
importOptions('$APPCONFIGOPTS/Gauss/VertexSmear_SMOG_pHe.py')

