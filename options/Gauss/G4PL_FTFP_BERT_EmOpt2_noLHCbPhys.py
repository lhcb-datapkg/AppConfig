##
##  File containing options to activate the FTFP_BERT Hadronic
##  Physics List with EM Opt2 in Geant4 and deactivate the simulation 
##  of Cherenkov photons and phot-electrons.
##

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'Opt2', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":False}
