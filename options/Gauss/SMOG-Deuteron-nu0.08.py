# File for setting SMOG injection of Deuteron
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Assuming Instantaneous luminosity L(pD2)=3 x 10^31 cm-2 s-1 with Nbb=2400 
#  Correspnding to nu(pD2) = 0.08, and mu = 0.7*nu = 0.056
#  
Gauss().FixedTargetParticle = 'd'
Gauss().FixedTargetLuminosity = 12.5*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 71.64*SystemOfUnits.millibarn
