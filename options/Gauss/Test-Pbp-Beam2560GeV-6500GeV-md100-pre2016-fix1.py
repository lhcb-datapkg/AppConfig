from Gauss.Configuration import *

Gauss().BeamMomentum = 2560*SystemOfUnits.GeV
Gauss().B2Momentum = -6500*SystemOfUnits.GeV
Gauss().B1Particle = 'Pb'
Gauss().B2Particle = 'p'

#############################################################
#HERE WE ASSUME THE SAME CONDITIONS AS 2013!!! TO BE UPDATED#
#############################################################

Gauss().Luminosity        = 0.543*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 3861*SystemOfUnits.millibarn

#--Set the average position of the IP
Gauss().InteractionPosition = [  0.885*SystemOfUnits.mm ,
                                -0.143*SystemOfUnits.mm ,
                                -12.19*SystemOfUnits.mm ]


#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 57.37*SystemOfUnits.mm


#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. And tilts of the beam line
Gauss().BeamHCrossingAngle = -0.325*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.00166*SystemOfUnits.mm
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
