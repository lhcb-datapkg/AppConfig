# File of beam condition for expected-2024 PbPb 
# Fixed InteractionPosition to be the same as expected-2024 pp

from Gauss.Configuration import *

#--Set the energy of the beams,
Gauss().BeamMomentum = 2680*SystemOfUnits.GeV
Gauss().B2Momentum = -2680*SystemOfUnits.GeV
Gauss().B1Particle = 'Pb'
Gauss().B2Particle = 'Pb'

#--Not used since OneFixedInteraction.py is called
Gauss().Luminosity        = 0.276*(10**25)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 8600*SystemOfUnits.millibarn

#--Set the average position of the IP
Gauss().InteractionPosition = [  1.1947*SystemOfUnits.mm,
                                 -0.0185*SystemOfUnits.mm,
                                 -1.9342*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
# \sigma_PV(Z) = 46.89 mm -> \sigma_beam(Z) = 66.31 mm
Gauss().BunchRMS = 66.3*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.274*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle = -0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0390 mm 
Gauss().BeamEmittance     = 0.0023*SystemOfUnits.mm 
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
