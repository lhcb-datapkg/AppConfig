# File for setting Beam conditions for MC09
#
# Beam7TeV
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/MC09-b7TeV-md100-nu07.py
#              $APPCONFIGOPTS/Conditions/MC09-20090602-vc-md100.py
#              $DECFILESROOT/options/30000000.opts (ie. event type)
#              $LBPYTHIAROOT/options/Pythia.opts (i.e. production engine)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Gauss.Configuration import *

#--Set the L/nbb, total cross section and revolution frequency and configure
#--the pileup tool
Gauss().Luminosity        = 10.0*(10**32)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().CrossingRate      = 30.2*SystemOfUnits.megahertz
Gauss().TotalCrossSection = 102.3*SystemOfUnits.millibarn

#--Set the luminous region for colliding beams and beam gas and configure
#--the corresponding vertex smearing tools, the choice of the tools is done
#--by the event type
Gauss().InteractionSize = [ 0.039*SystemOfUnits.mm, 0.039*SystemOfUnits.mm,
                            5.34*SystemOfUnits.cm ]
Gauss().BeamSize        = [ 0.055*SystemOfUnits.mm, 0.055*SystemOfUnits.mm ]

#--Set the energy of the beam,
#--the half effective crossing angle (in LHCb coordinate system),
#--beta* and emittance
Gauss().BeamMomentum      = 7.0*SystemOfUnits.TeV
Gauss().BeamCrossingAngle = 0.200*SystemOfUnits.mrad
Gauss().BeamEmittance     = 0.503*(10**(-9))*SystemOfUnits.rad*SystemOfUnits.m
Gauss().BeamBetaStar      = 6.0*SystemOfUnits.m

#--Set the bunch spacing to 25 ns, hence spill-over for the following slots
Gauss().SpilloverPaths = [ 'Prev', 'PrevPrev', 'Next' ]

#--Starting time, to identify beam conditions, all events will have the same
ec = EventClockSvc()
ec.addTool(FakeEventTime(), name="EventTimeDecoder")
ec.EventTimeDecoder.StartTime = 203.0*SystemOfUnits.ms
ec.EventTimeDecoder.TimeStep  = 0.0

