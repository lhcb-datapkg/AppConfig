# File for setting SMOG injection of Hydrogen during 2024.W25.27 - Block4.
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-He)= 1.66862 x 10^30 cm^-2s^-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb =  6.9526 x 10^26 cm-2 s-1,
#  corresponds to nu(p-He)=0.00726, mu(p-He)=0.00508.

Gauss().FixedTargetParticle = 'He'
Gauss().FixedTargetLuminosity = 6.95*(10**26)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 117.43*SystemOfUnits.millibarn
