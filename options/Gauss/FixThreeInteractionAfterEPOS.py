
# config file to overlay 2 EPOS interactions for pA interactions 
# contact Hengne.Li@cern.ch


def FixThreeInteractionAfterEPOS():
    from Configurables import Generation
    Generation().FixedNInteractions.NInteractions = 3

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(FixThreeInteractionAfterEPOS)


