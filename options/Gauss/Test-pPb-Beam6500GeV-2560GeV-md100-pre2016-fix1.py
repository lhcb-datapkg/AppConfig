from Gauss.Configuration import *

Gauss().BeamMomentum = 6500*SystemOfUnits.GeV
Gauss().B2Momentum = -2560*SystemOfUnits.GeV
Gauss().B1Particle = 'p'
Gauss().B2Particle = 'Pb'

#--Not used since OneFixedInteraction.py is called
Gauss().Luminosity        = 1.086*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 3866*SystemOfUnits.millibarn

#--Set the average position of the IP
Gauss().InteractionPosition = [  0.867*SystemOfUnits.mm ,
                                -0.161*SystemOfUnits.mm ,
                                -12.21*SystemOfUnits.mm ]


#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 62.17*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. And tilts of the beam line
Gauss().BeamHCrossingAngle =  -0.325*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.00166*SystemOfUnits.mm
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')
