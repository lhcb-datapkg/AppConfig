# File for setting hypotetical Beam conditions
# They are average conditions for 2017 running:
#   Beam 6.5 TeV, beta* = 3m , emittance(normalized) ~ 6.5 micron
#   External Horizontal Crossing Angle
#   No spill-over
#   To switch on the spill-over a dedicate file has to be added
#   Magnet down
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2017-nu0.1.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch for a special run with low mu, 202694
#  It correspond to mu(visible) = 0.07 --> nu(total) = mu/0.69 = 0.101
Gauss().Luminosity        = 0.0112*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 101.5*SystemOfUnits.millibarn

#--Set the average position of the IP: from Niels Tuning, LHCBGAUSS-1481
Gauss().InteractionPosition = [  0.8450*SystemOfUnits.mm,
                                -0.1104*SystemOfUnits.mm,
                                -6.6084*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 62.588*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 6.5*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.105*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle =  0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.031 mm ~ 31 um
Gauss().BeamEmittance     = 0.006515*SystemOfUnits.mm
Gauss().BeamBetaStar      = 3.0*SystemOfUnits.m

#--Set up to use beam conditions in Signal Particle Guns
from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/VertexSmear_SignalPGun.py')
