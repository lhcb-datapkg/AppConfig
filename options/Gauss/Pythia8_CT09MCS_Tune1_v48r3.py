from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import ( SignalPlain, SignalRepeatedHadronization, Special )
from Configurables import Pythia8Production
from Configurables import BcVegPyProduction, GenXiccProduction


commandsTuning_1 = [
    'Init:showAllSettings                = on',

    'SpaceShower:rapidityOrder           = off',          # General
    'SpaceShower:alphaSvalue             = 0.130',
    'MultipartonInteractions:alphaSvalue = 0.130',
    
    'StringFlav:mesonSvector             = 7.474387e-01', # Flavour selection
    'StringFlav:probQQtoQ                = 1.615701e-01',
    'StringFlav:probStoUD                = 3.501613e-01',
    
    'MultipartonInteractions:pT0Ref      = 2.742289e+00'  # Multiparton interactions
    ]

Pythia8TurnOffFragmentation = [ "HadronLevel:all = off" ]

gaussGen = Generation("Generation")

gaussGen.addTool(MinimumBias, name = "MinimumBias")
gaussGen.MinimumBias.ProductionTool = "Pythia8Production"
gaussGen.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.MinimumBias.Pythia8Production.Commands += commandsTuning_1
gaussGen.MinimumBias.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"

gaussGen.addTool(Inclusive, name = "Inclusive")
gaussGen.Inclusive.ProductionTool = "Pythia8Production"
gaussGen.Inclusive.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Inclusive.Pythia8Production.Commands += commandsTuning_1
gaussGen.Inclusive.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"

gaussGen.addTool(SignalPlain, name = "SignalPlain")
gaussGen.SignalPlain.ProductionTool = "Pythia8Production"
gaussGen.SignalPlain.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalPlain.Pythia8Production.Commands += commandsTuning_1
gaussGen.SignalPlain.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"

gaussGen.addTool(SignalRepeatedHadronization, name = "SignalRepeatedHadronization")
gaussGen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gaussGen.SignalRepeatedHadronization.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalRepeatedHadronization.Pythia8Production.Commands += commandsTuning_1
gaussGen.SignalRepeatedHadronization.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"
gaussGen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffFragmentation

gaussGen.addTool(Special, name = "Special")
gaussGen.Special.ProductionTool = "Pythia8Production"
gaussGen.Special.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Special.Pythia8Production.Commands += commandsTuning_1
gaussGen.Special.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"
gaussGen.Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
gaussGen.Special.addTool(Pythia8Production, name = "Pythia8PileUp")
gaussGen.Special.Pythia8PileUp.Tuning = "LHCbDefault_CT09MCS.cmd"
gaussGen.Special.Pythia8PileUp.Commands += commandsTuning_1
gaussGen.Special.ReinitializePileUpGenerator  = False

gaussGen.Special.addTool(BcVegPyProduction)
gaussGen.Special.BcVegPyProduction.addTool(Pythia8Production)
gaussGen.Special.BcVegPyProduction.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"
gaussGen.Special.BcVegPyProduction.Pythia8Production.Commands += commandsTuning_1

gaussGen.Special.addTool(GenXiccProduction)
gaussGen.Special.GenXiccProduction.addTool(Pythia8Production)
gaussGen.Special.GenXiccProduction.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"
gaussGen.Special.GenXiccProduction.Pythia8Production.Commands += commandsTuning_1

from Configurables import Gauss
spillOverList = Gauss().getProp("SpilloverPaths")

for slot in spillOverList:
    genSlot = Generation("Generation"+slot)
    genSlot.addTool(MinimumBias, name = "MinimumBias")
    genSlot.MinimumBias.ProductionTool = "Pythia8Production"
    genSlot.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
    genSlot.MinimumBias.Pythia8Production.Commands += commandsTuning_1
    genSlot.MinimumBias.Pythia8Production.Tuning = "LHCbDefault_CT09MCS.cmd"

