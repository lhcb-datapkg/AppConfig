# Beam file for 2022 pAr, specifying the FixTargetParticle and nu for pAr case, 
# other beam parameters are the same as Beam6800GeV-md100-2022-nu2.1.py.
# Thus, this beam file should be used together with Beam6800GeV-md100-2022-nu2.1.py,
# And requires Gauss v56r0 or higher.
#
#  Syntax is: 
#  gaudirun.py  $APPCONFIGOPTS/Gauss/Beam6800GeV-md100-2022-nu2.1.py
#               $APPCONFIGOPTS/Gauss/pAr-Beam6800GeV-0GeV-md100-2022-0.37.py
#               $GAUSSOPTS/BeforeVeloGeometry.py
#       	$APPCONFIGOPTS/Gauss/Smog2TriangularProfile.py
#		$GAUSSOPTS/BeamGas.py #(only pAr; if pAr+pp, change to: $GAUSSOPTS/BeamGasWithBeamBeam.py)
#		$LBCRMCROOT/options/EPOSBeamGas.py  #(EPOS for BeamGas)
#               $DECFILESROOT/options/30000000.py #(i.e. event type, if embeding, add $GAUSSOPTS/Embedding.py)
#		$APPCONFIGOPTS/Gauss/Run3-detector-NoUT.py;
#		$APPCONFIGOPTS/Gauss/DataType-2022.py;
#		$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py
#		$GAUSSOPTS/GenStandAlone.py


from Gauss.Configuration import *

# Specify beam particle
Gauss().B2Momentum = -6800*SystemOfUnits.GeV
Gauss().B1Particle = 'p'
Gauss().B2Particle = 'p'
Gauss().FixedTargetParticle = 'Ar'


#  assuming L(beam-gas)=1.8 x 10^31 cm-2 s-1 with 2600 beam1 bunches
#  corresponds to nu(beam-gas)=0.37
Gauss().FixedTargetLuminosity = 7*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 600.*SystemOfUnits.millibarn


