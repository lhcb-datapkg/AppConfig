#-- Enable spill-over 
#-- Set the bunch spacing to 25 ns, hence spill-over for the following slots
from Configurables import Gauss
Gauss().SpilloverPaths = [ 'PrevPrev',
                           'Prev',
                           'Next',
                           'NextNext' ]

from Configurables import GenInit
from GaudiKernel import SystemOfUnits
GenInit("GaussGen").BunchSpacing = 25 * SystemOfUnits.ns

