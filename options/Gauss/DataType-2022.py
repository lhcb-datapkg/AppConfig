##############################################################################
# File for running Gauss on 2022 detector configuration
##############################################################################

from Configurables import Gauss
Gauss().DataType  = "2022"
