
# config file to overlay 3 EPOS interactions for Ap interactions
# contact Hengne.Li@cern.ch

def FixFourInteractionAfterEPOS():
    from Configurables import Generation
    Generation().FixedNInteractions.NInteractions = 4

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(FixFourInteractionAfterEPOS)
