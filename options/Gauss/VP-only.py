############################################################################
# File for running Gauss with just the VP detector and the magnet in order
# to account for fringe magnet field. Designed for Run 3.
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['VP', 'Magnet'] }
Gauss().DetectorSim  = { "Detectors": ['VP', 'Magnet'] }
Gauss().DetectorMoni = { "Detectors": ['VP'] }