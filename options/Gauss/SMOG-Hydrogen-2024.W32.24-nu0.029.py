# File for setting SMOG injection of Hydrogen during 2024.W32.W34 - Block1.
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-H2) = 9.8521 x 10^30 cm^-2 s^-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb = 4.105 x 10^27 cm-2 s-1,
#  corresponds to nu(p-H2)=0.0287, mu(p-H2)=0.02.

Gauss().FixedTargetParticle = 'p'
Gauss().FixedTargetLuminosity = 4.105*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 78.56*SystemOfUnits.millibarn
