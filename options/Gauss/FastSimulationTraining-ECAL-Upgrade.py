###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# we are going to place an external plane in front of the zMax plane
# which for the upgrade is set to 11948. mm and tilted by .207 deg
# run with Gauss >= v55r4

from GaudiKernel.SystemOfUnits import m, mm, degree
zmax = 11948. * mm
collector_z_size = .01 * mm
collector_x_size = 10. * m
collector_y_size = 10. * m
collector_z_pos = zmax - collector_z_size / 2.
collector_angle = .207 * degree

# turn the ExternalDetectorEmbedder on, it is a tool
# that will insert an external plane in front of the calorimeter
from Configurables import ExternalDetectorEmbedder
external = ExternalDetectorEmbedder("EcalCollectorEmbedder")

# generating ecal collector plane
external.Shapes = {
    'EcalCollector': {
        "Type": "Cuboid",
        "zPos": collector_z_pos,
        "xSize": collector_x_size,
        "ySize": collector_y_size,
        "zSize": collector_z_size,
        "xAngle": -collector_angle,
    }
}

external.Sensitive = {
    'EcalCollector': {
        "Type": "MCCollectorSensDet",
        "RequireEDep": False,
        "OnlyForward": True,
        "OnlyAtBoundary": True,
    },
}

external.Hit = {
    'EcalCollector': {
        'Type': 'GetMCCollectorHitsAlg',
    },
}

external.Moni = {
    'EcalCollector': {
        'Type': 'CaloCollector',
        'CheckParent': True,
        'CollectorZ': collector_z_pos,
        'CollectorXAngle': collector_angle,
    },
}

# since the collector plane will overlap with other
# volumes, it has to be placed in a parallel world
from Configurables import ParallelGeometry, Gauss
Gauss().ParallelGeometry = True
ParallelGeometry().ParallelWorlds = {
    'EcalCollectorParallelWorld': {
        'ExternalDetectorEmbedder': 'EcalCollectorEmbedder',
    },
}

ParallelGeometry().ParallelPhysics = {
    'EcalCollectorParallelWorld': {
        'LayeredMass': False,
    }
}

from Gaudi.Configuration import appendPostConfigAction, OutputStream


def addCollHitsToTape():
    OutputStream("GaussTape").ItemList += ["/Event/MC/EcalCollector/Hits#1"]


appendPostConfigAction(addCollHitsToTape)
