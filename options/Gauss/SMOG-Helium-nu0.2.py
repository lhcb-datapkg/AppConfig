# File for setting SMOG injection of Helium
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  assuming L(beam-gas)=5.2 x 10^31 cm-2 s-1 with 2600 beam1 bunches
#  corresponds to nu(beam-gas)=0.2
Gauss().FixedTargetParticle = 'He'
Gauss().FixedTargetLuminosity = 2*(10**28)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 110.*SystemOfUnits.millibarn
