# File for setting SMOG injection of Nitrogen
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Assuming Instantaneous luminosity L(pN2)=1.5 x 10^31 cm-2 s-1 with Nbb=2400 
#  Correspnding to nu(pN2) = nu0.168, and mu = 0.7*nu = 0.118
#  
Gauss().FixedTargetParticle = 'N'
Gauss().FixedTargetLuminosity = 6.25*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 302.2*SystemOfUnits.millibarn
