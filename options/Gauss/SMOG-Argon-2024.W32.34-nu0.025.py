# File for setting SMOG injection of Argon during 2024.W32.34 - Block1.
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-Ar) = 1.096412 x 10^30 cm-2 s-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb = 4.5684 x 10^26 cm-2 s-1,
#  corresponds to nu(p-Ar)=0.0251, mu(p-Ar)=0.0175.

Gauss().FixedTargetParticle = 'Ar'
Gauss().FixedTargetLuminosity = 4.57*(10**26)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 617*SystemOfUnits.millibarn
