# File for setting SMOG injection of Hydrogen 
# This can only be used together with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Assuming Instantaneous luminosity L(pH2)=6 x 10^31 cm-2 s-1 with Nbb=2400 
#  Correspnding to nu(pH2) = 0.087, and mu = 0.7*nu = 0.061#    
Gauss().FixedTargetParticle = 'p'
Gauss().FixedTargetLuminosity = 25*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 39.28*SystemOfUnits.millibarn
