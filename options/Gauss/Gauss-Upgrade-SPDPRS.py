# $Id: Upgrade.py,v 1.1 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Gauss with Upgrade configuration
##############################################################################

from Gauss.Configuration import *
from Configurables import Gauss

for det in ["Spd", "Prs"]:
    if det not in Gauss().DetectorGeo["Detectors"]:
        Gauss().DetectorGeo["Detectors"].append(det)

for det in ["Spd", "Prs"]:
    if det not in Gauss().DetectorSim["Detectors"]:
        Gauss().DetectorSim["Detectors"].append(det)

for det in ["Spd", "Prs"]:
    if det not in Gauss().DetectorMoni["Detectors"]:
        Gauss().DetectorMoni["Detectors"].append(det)



