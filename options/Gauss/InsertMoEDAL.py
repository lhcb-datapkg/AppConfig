from Gaudi.Configuration import *

from Configurables import Gauss

def MoEDALgeometry():
    from Configurables import GiGaInputStream
    geo = GiGaInputStream('Geo')
    geo.StreamItems      += ["/dd/Structure/LHCb/MagnetRegion/MoedalNtdTT"]

appendPostConfigAction(MoEDALgeometry)
