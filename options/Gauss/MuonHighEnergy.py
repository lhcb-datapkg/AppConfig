## ############################################################################
## # File for running Gauss Gaussv56r7 (Sim10)  with Run3 detectors and High Energy
## # configuration for Muon Background
## #
## # E. De Lucia
## ############################################################################
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions, appendPostConfigAction

def muonHighEnergyXMLSim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonHighEnergy.xml"
appendPostConfigAction(muonHighEnergyXMLSim)
