# File for setting SMOG injection of Xenon
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Assuming Instantaneous luminosity L(pXe)=0.9 x 10^31 cm-2 s-1 with Nbb=2400 
#  Correspnding to nu(pKr) = 0.446, and mu = 0.7*nu = 0.312
#  
Gauss().FixedTargetParticle = 'Xe'
Gauss().FixedTargetLuminosity = 3.75*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 1337.15*SystemOfUnits.millibarn
