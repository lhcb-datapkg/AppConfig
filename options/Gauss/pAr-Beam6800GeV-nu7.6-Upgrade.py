# Beam file for Run3 Upgrade pAr simulation
# Requires Gauss v56r0 or higher
# Gauss MR: MR!857
#
from Gauss.Configuration import *

Gauss().BeamMomentum = 6800*SystemOfUnits.GeV
Gauss().B2Momentum = -6800*SystemOfUnits.GeV
Gauss().B1Particle = 'p'
Gauss().B2Particle = 'p'
Gauss().FixedTargetParticle = 'Ar'

#  assuming L(beam-beam) = 2 x 10^33 cm-2 s-1 with 2400 colliding bunches
#  It correspond to nu(total) = 7.6 and we assume mu(visible)=0.699*nu
Gauss().Luminosity        = 0.8338*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
#0.177*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn
#155*SystemOfUnits.millibarn


Gauss().FixedTargetLuminosity = 7*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
#  assuming L(beam-gas)=1.8 x 10^31 cm-2 s-1 with 2600 beam1 bunches
Gauss().FixedTargetXSection = 600.*SystemOfUnits.millibarn
#  corresponds to nu(beam-gas)=0.37

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [0.0,   # 0.810*SystemOfUnits.mm,
                               0.0,   #-0.220*SystemOfUnits.mm,
                               0.0, ]  # 0.000*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 90.00*SystemOfUnits.mm
#53.60*SystemOfUnits.mm

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depends on the polarity (negative angle for magnet down)
#  The angle given is for beam 1, the one for beam 2 is assuming the opposite
#  sign in the code.
Gauss().BeamHCrossingAngle = -0.115*SystemOfUnits.mrad #-0.490*SystemOfUnits.mrad
Gauss().BeamVCrossingAngle = -0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 3m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
# Gives \sigma_{x,y} = sqrt(beta* x emittance) = 0.0329 mm ~ 33 um
# values for 2016 fills for VdM scan, May 2016:
Gauss().BeamEmittance     = 0.0038*SystemOfUnits.mm#0.0029*SystemOfUnits.mm
Gauss().BeamBetaStar      = 2.79*SystemOfUnits.m
#24.0*SystemOfUnits.m

from Gaudi.Configuration import importOptions
#importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

