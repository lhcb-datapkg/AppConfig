##############################################################################
# File for running Gauss with MC09 configuration and beam conditions as in
# production (5 TeV beams, nu=1, no spill-over)
#
# Syntax is:
#   gaudirun.py Gauss-MC09.py <someInputJobConfiguration>.py
##############################################################################

from Gaudi.Configuration import *
from Gauss.Configuration import *


importOptions("$APPCONFIGOPTS/Conditions/RichAerogelRemoveInDB.py")



def noAerogel():
    from Configurables import SimulationSvc    
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"
    importOptions("$GAUSSRICHROOT/options/RichRemoveAerogel.opts")
appendPostConfigAction( noAerogel )









