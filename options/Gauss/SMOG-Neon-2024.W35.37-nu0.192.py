# File for setting SMOG injection of Neon during 2024.W35.37 - Block5. 
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-Ne) = 1.339696 x 10^31 cm-2 s-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb = 5.5821 x 10^27 cm-2 s-1,
#  Correspnding to nu(p-Ne)=0.192, and mu(p-Ne) = 0.134.
#  
Gauss().FixedTargetParticle = 'Ne'
Gauss().FixedTargetLuminosity = 5.58*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 386.68*SystemOfUnits.millibarn
