#-- Fix one interaction per bunch crossing!
from Configurables import Generation
from Configurables import FixedNInteractions
Generation("Generation").PileUpTool = "FixedNInteractions"
Generation("Generation").addTool( FixedNInteractions )
Generation("Generation").FixedNInteractions.NInteractions = 1
