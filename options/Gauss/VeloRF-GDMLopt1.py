# $Id: GdmlRead.py,v 1.1 2013-01-02 10:32:13 mreid Exp $
##############################################################################
# File for importing GDML geometry and turning off XML description
##############################################################################

from Configurables import Gauss

Gauss().ReplaceWithGDML  = [{ "volsToReplace": ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFFoilRight"], "gdmlFile" : "$GDMLDATAROOT/data/Right_Mesh_Safe.gdml"},
                            { "volsToReplace" : ["/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFFoilLeft"], "gdmlFile" : "$GDMLDATAROOT/data/Left_Mesh_Safe.gdml" }]
