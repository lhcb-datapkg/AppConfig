from Configurables import Gauss, ConversionFilter
from GaudiKernel import SystemOfUnits
ConversionFilter = ConversionFilter("ConversionFilter")
ConversionFilter.MaxZ = 2500. * SystemOfUnits.mm
