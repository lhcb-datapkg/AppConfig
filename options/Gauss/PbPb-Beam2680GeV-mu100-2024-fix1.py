# File for setting realistic PbPb Beam conditions for post-TS1 2024
# Fixed InteractionPosition to be the same as pp ref run 2024

from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the type of beams
Gauss().B1Particle        = 'Pb'
Gauss().B2Particle        = 'Pb'

#--Set the energy of the beams
Gauss().BeamMomentum      = 2680*SystemOfUnits.GeV
Gauss().B2Momentum        = -2680*SystemOfUnits.GeV

#  Expected luminosity leveling in IP8: 1.5 × 10^27 cm−2 s−1 
#  Instantaneous luminosity per bunch crossing with 1240 colling bunches = 0.121 x 10^25 cm-2 s-1
#  Pile-up should be around 9.25 x 10^-4, much below one, thus fix one interaction
# This part is ignored since OneFixedInteraction is called below.
Gauss().Luminosity        = 0.121*(10**25)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 8600*SystemOfUnits.millibarn

#--Set the average position of the IP:
#  Average IP position from PbPb 2024 data: https://gitlab.cern.ch/lhcb-simulation/mc-productions-setup/-/issues/51
Gauss().InteractionPosition = [ 0.417*SystemOfUnits.mm,
                                0.114*SystemOfUnits.mm,
                               8.495*SystemOfUnits.mm ]

#--Set the bunch RMS from the experimental sigmaZ of the Pb beams from measured 2024 PbPb data: 
# https://gitlab.cern.ch/lhcb-simulation/mc-productions-setup/-/issues/51
# sqrt(2)*sigma_Z=sqrt(2)*63.403mm=89.6654mm
Gauss().BunchRMS = 89.6654*SystemOfUnits.mm

# The crossing angle for pp reference and PbPb 2024 (MagUp) is fully horizontal.
# The magnet polarity is -/UP.
# Angles taken from: https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/merge_requests/278#note_8935378
Gauss().BeamHCrossingAngle = ((-0.210)+(+0.139))*SystemOfUnits.mrad #(external+internal) 
Gauss().BeamVCrossingAngle = 0*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance for PbPb 2024:
# beta* = 1.5m is taken from https://lpc.web.cern.ch/Run3/HIConfiguration2024.html
# Preliminary sigmaX and sigmaY for real PbPb 2024 by the lumi group:  39 (V) and 52 (H) micron,
# related to the transverse beam spot size by Σx2 = 2σx2cos2(α/2) + 2σz2sin2(α/2), Σy2 = 2σy2.  
# Therefore an estimated beamspot size of 24 micron is used.
# Given gamma = E/M = sqrt(s)/(2Am_p) = Asqrt(sNN)/(2Am_p) = sqrt(sNN)/(2m_p) = 5360GeV/(2*0.938272013) = 2856.3145,
# Given beta = sqrt(1-1/gamma^2) = 0.99999994,
# Given sigma = sqrt(beta*/2*epsilonN*beta/gamma) = 24 micron, 
# The normalised emittance epsilonN = sigma^2/(beta*/2 * beta/gamma) = 0.0021936 mm.
# See also: https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/merge_requests/278#note_8935378
Gauss().BeamEmittance     = 0.00219*SystemOfUnits.mm
Gauss().BeamBetaStar      = 1.5*SystemOfUnits.m

#One fixedinteraction due to pile-up below 1
from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

