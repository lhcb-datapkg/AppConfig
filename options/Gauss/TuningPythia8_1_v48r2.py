from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import ( SignalPlain, SignalRepeatedHadronization, Special )
from Configurables import Pythia8Production


commandsTuning_1 = [
    'Init:showAllSettings                = on',

    'SpaceShower:rapidityOrder           = off',          # General
    'SpaceShower:alphaSvalue             = 0.130',
    'MultipartonInteractions:alphaSvalue = 0.130',
    
    'StringFlav:mesonSvector             = 7.474387e-01', # Flavour selection
    'StringFlav:probQQtoQ                = 1.615701e-01',
    'StringFlav:probStoUD                = 3.501613e-01',
    
    'MultipartonInteractions:pT0Ref      = 2.742289e+00'  # Multiparton interactions
    ]

gaussGen = Generation("Generation")

gaussGen.addTool(MinimumBias, name = "MinimumBias")
gaussGen.MinimumBias.ProductionTool = "Pythia8Production"
gaussGen.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.MinimumBias.Pythia8Production.Commands += commandsTuning_1

gaussGen.addTool(Inclusive, name = "Inclusive")
gaussGen.Inclusive.ProductionTool = "Pythia8Production"
gaussGen.Inclusive.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Inclusive.Pythia8Production.Commands += commandsTuning_1

gaussGen.addTool(SignalPlain, name = "SignalPlain")
gaussGen.SignalPlain.ProductionTool = "Pythia8Production"
gaussGen.SignalPlain.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalPlain.Pythia8Production.Commands += commandsTuning_1

gaussGen.addTool(SignalRepeatedHadronization, name = "SignalRepeatedHadronization")
gaussGen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gaussGen.SignalRepeatedHadronization.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.SignalRepeatedHadronization.Pythia8Production.Commands += commandsTuning_1

gaussGen.addTool(Special, name = "Special")
gaussGen.Special.ProductionTool = "Pythia8Production"
gaussGen.Special.addTool(Pythia8Production, name = "Pythia8Production")
gaussGen.Special.Pythia8Production.Commands += commandsTuning_1

