##############################################################################
# File for running Gauss on 2023 detector configuration
##############################################################################

from Configurables import Gauss
Gauss().DataType  = "2023"
