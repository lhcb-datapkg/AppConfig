# File for setting hypotetical Beam conditions
# They are suitable for upgrade studies:
#   Beam 6.8 TeV, beta* = 2m , emittance(normalized) ~ 2.5 micron
#   No spill-over
# 
#
# Requires Gauss v45r1 or higher.
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam6800GeV-mu100-2022-nu3.2.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Sim08-2011-Tags.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch, corresponding to mu = 2.2 of fill 8496
#  It correspond to nu(total) = 3.19 and we assume mu(visible)=0.699*nu
Gauss().Luminosity        = 0.35*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn

#--Set the average position of the IP: assume a perfectly centered beam in LHCb
Gauss().InteractionPosition = [ 1.092*SystemOfUnits.mm,
                                0.474*SystemOfUnits.mm,
                                0.65*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region. SigmaX and SigmaY are calculated from the beta* and
#  emittance
Gauss().BunchRMS = 73.54*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 6.8*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
Gauss().BeamHCrossingAngle = ((+0.145)+(-0.200))*SystemOfUnits.mrad # internal + external
Gauss().BeamVCrossingAngle = 0.000*SystemOfUnits.mrad
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 2m and e_norm 2.5um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.0212*SystemOfUnits.mm
# Gives \sigma_{x,y} =  0.0540851 ~= 0.054 mm
Gauss().BeamBetaStar      = 2.00*SystemOfUnits.m
