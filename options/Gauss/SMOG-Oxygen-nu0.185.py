# File for setting SMOG injection of Oxygen
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Assuming Instantaneous luminosity L(pO2)=1.5 x 10^31 cm-2 s-1 with Nbb=2400 
#  Correspnding to nu(pO2) = 0.185, and mu = 0.7*nu = 0.13
#  
Gauss().FixedTargetParticle = 'O'
Gauss().FixedTargetLuminosity = 6.25*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 333.7*SystemOfUnits.millibarn
