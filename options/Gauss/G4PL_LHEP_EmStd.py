##
##  File containing options to activate the Standard Em
##  Physics in Geant4 (the default for production is Em
##  Option1)
##

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'Std', "Hadron":'LHEP', "GeneralPhys":True, "LHCbPhys":True}
