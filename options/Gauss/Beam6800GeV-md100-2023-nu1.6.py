# File for setting hypotetical Beam conditions for 2023
#
# Syntax is: 
#  gaudirun.py $APPCONFIGOPTS/Gauss/Beam6800GeV-md100-2023-nu1.4.py
#              $DECFILESROOT/options/30000000.opts (i.e. event type)
#              $LBGENROOT/options/GEN.py (i.e. production engine)
#              Tags-YEAR.py (i.e. database tags to be used)
#              gaudi_extra_options_NN_II.py (ie. job specific: random seed,
#                               output file names, see Gauss-Job.py as example)
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#--Set the L/nbb, total cross section and revolution frequency and configure
#  the pileup tool, a CrossingRate of 11.245 kilohertz is used internally
#  This is the L per bunch, corresponding to mu = 1.0
#  It correspond to nu(total) = 1.43 and we assume mu(visible)=0.699*nu
Gauss().Luminosity        = 0.175*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().TotalCrossSection = 102.5*SystemOfUnits.millibarn

#--Set the average position of the IP:
# assumes a perfectly centered beam in z and same shift in x,y as in 2022
Gauss().InteractionPosition = [ 1.29*SystemOfUnits.mm,
                                0.57*SystemOfUnits.mm,
                                7.8*SystemOfUnits.mm ]

#--Set the bunch RMS, this will be used for calculating the sigmaZ of the
#  Interaction Region, Bunch RMS = sqrt(2)*PV_Z_RMS
#  Nominally it is 90mm (1.2 ns) but it is reduced due to Geometrical Factor
#  SigmaX and SigmaY are calculated from the beta* and emittance 
Gauss().BunchRMS = 63.36*SystemOfUnits.mm

#--Set the energy of the beam,
Gauss().BeamMomentum      = 6.8*SystemOfUnits.TeV

#--the half effective crossing angle (in LHCb coordinate system), horizontal
#  and vertical. The horizontal one is given by the LHCb magnet and corrector
#  so its sign depend on the polarity (negative angle for magnet down)
Gauss().BeamHCrossingAngle = -0.145*SystemOfUnits.mrad # internal
Gauss().BeamVCrossingAngle = +0.200*SystemOfUnits.mrad # external
Gauss().BeamLineAngles     = [ 0.0, 0.0 ]

#--beta* and emittance (beta* is nomimally 2m and e_norm 25um,
#                       adjusted to match sigmaX and sigmaY)
Gauss().BeamEmittance     = 0.01845*SystemOfUnits.mm
# Gives \sigma_{x,y}(beam) = 0.035 mm ~= \sigma_{x,y}(PV) = 0.0505 mm
Gauss().BeamBetaStar      = 2.0*SystemOfUnits.m
