############################################################################
# File for running Gauss with only Run3 Calorimeters
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['Ecal', 'Hcal', 'Magnet'] }
Gauss().DetectorSim  = { "Detectors": ['Ecal', 'Hcal', 'Magnet'] }
Gauss().DetectorMoni = { "Detectors": ['Ecal', 'Hcal'] }
Gauss().BeamPipe     = "BeamPipeInDet"

Gauss().DataType = "Upgrade"
