##############################################################################
# File for running Gauss on 2024 detector configuration
##############################################################################

from Configurables import Gauss
Gauss().DataType  = "2024"
