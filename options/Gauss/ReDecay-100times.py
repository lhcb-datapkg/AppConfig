#-- Activates the redecay of particles options with 100 redecay per original
#-- event. By default, every particle stable in the production tool which
#-- is heavier than the signal particle is redecayed.

from Configurables import Gauss

Gauss().Redecay['active'] = True
Gauss().Redecay['N'] = 100
