# $Id: Upgrade.py,v 1.1 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Gauss with Upgrade configuration
##############################################################################

from Gauss.Configuration import *


from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True

if "Calo_NoSPDPRS" not in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType += ["Calo_NoSPDPRS"]
 
from Configurables import Gauss
for det in ["Spd", "Prs"]:
    if det in Gauss().DetectorGeo["Detectors"]:
        Gauss().DetectorGeo["Detectors"].remove(det)

for det in ["Spd", "Prs"]:
    if det in Gauss().DetectorSim["Detectors"]:
        Gauss().DetectorSim["Detectors"].remove(det)

for det in ["Spd", "Prs"]:
    if det in Gauss().DetectorMoni["Detectors"]:
        Gauss().DetectorMoni["Detectors"].remove(det)

Gauss().DataType = "Upgrade"

