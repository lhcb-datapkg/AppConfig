# File for setting SMOG injection of Hydrogen during 2024.W31 - Block2. 
#  
# This can only be used togheter with optiosn already specifying the beam particles
# e.g. The only thing specific to a year may be the luminosity and xsection
#
from Configurables import Gauss
from GaudiKernel import SystemOfUnits

#  Ins_L(p-H2) = 8.7213 x 10^30 cm^-2 s^-1 with 2400 beam1 bunches,
#  thus FixedTargetLuminosity = Ins_L/n_bb = 3.6339 x 10^27 cm-2 s-1,
#  corresponds to nu(p-H2)=0.0254, mu(p-H2)=0.0177.


Gauss().FixedTargetParticle = 'p'
Gauss().FixedTargetLuminosity = 3.87*(10**27)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 78.56*SystemOfUnits.millibarn
