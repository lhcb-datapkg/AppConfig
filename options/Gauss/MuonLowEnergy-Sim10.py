## ############################################################################
## # File for running Gauss Gaussv56r7 (Sim10)  with Run3 detectors and Low Energy
## # configuration for Muon Background Parameterization
## #
## # E. De Lucia 
## ############################################################################
from Configurables import Gauss
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import importOptions, appendPostConfigAction

## Load the cavern geometry and the downstream magnet
#
from Configurables import GaussGeo
geo = GaussGeo()
geo.GeoItemsNames += ["/dd/Structure/Infrastructure"]
geo.GeoItemsNames += ["/dd/Structure/LHCb/DownstreamRegion/AfterMuon"]

## Need to give "FTFP_BERT_HP_NoCherenkov" option separately - keep it
## separate to allow using it with other Physics List if/when needed
## Physics Setup
# - FTFP-BERT with HP hadronic physics list
# - RICH Cherenkov OFF 
# - One fixed interaction
# - Production cuts applied
# - Tracking cuts applied
# - Muon filter physics thresholds overridden
##

## Muon Low energy - it should include the options from Gauss
importOptions('$GAUSSOPTS/MuonLowEnergy.py')
def muonLowEnergyXMLSim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"
appendPostConfigAction(muonLowEnergyXMLSim)
