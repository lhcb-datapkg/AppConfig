"""
Special hack to enable the python custom main loop in old versions of gaudirun.py

Usage: add this option file early in the list of options passed to gaudirun.py

   gaudirun.py EnableCustomMainLoop.py myRegularOptions.py

"""
from __future__ import print_function
__author__ = "Marco Clemencic <marco.clemencic@cern.ch>"

import Gaudi.Configuration

if not hasattr(Gaudi.Configuration, "setCustomMainLoop"):
    def setCustomMainLoop(runner):
        '''
        Replace the default main execution loop with the specified callable object.

        @param runner: a callable that accepts an initialized instance of GaudiPython.AppMgr
                       and the number of events to process and returns a StatusCode
        '''
        # change the mainLoop function
        from Gaudi.Main import gaudimain
        from inspect import getargspec
        # get the number of arguments of the runner function to be backward compatible
        # with the previous buggy implementation (it was expecting 3 arguments)
        try:
            nargs = len(getargspec(runner).args)
        except TypeError: # runner is an instance and not a function
            nargs = len(getargspec(runner.__call__).args) - 1
        print(nargs)
        if nargs == 2:
            gaudimain.mainLoop = lambda self, gaudi, nEvt: runner(gaudi, nEvt)
        else:
            gaudimain.mainLoop = lambda self, gaudi, nEvt: runner(self, gaudi, nEvt)
    # add the function to the module
    Gaudi.Configuration.setCustomMainLoop = setCustomMainLoop

    # this hack replaces the old version of gaudimain.runSerial with one that
    # uses the custom mainLoop
    from Gaudi.Main import gaudimain
    def runSerial(self):
        from time import time
        #--- Instantiate the ApplicationMgr------------------------------
        import GaudiPython
        self.log.debug('-'*80)
        self.log.debug('%s: running in serial mode', __name__)
        self.log.debug('-'*80)
        sysStart = time()
        self.g = GaudiPython.AppMgr()
        runner = self.mainLoop or (lambda app, nevt: app.run(nevt))
        success = runner(self.g, self.g.EvtMax).isSuccess()
        success = self.g.exit().isSuccess() and success
        if not success and self.g.ReturnCode == 0:
            # ensure that the return code is correctly set
            self.g.ReturnCode = 1
        sysTime = time()-sysStart
        self.log.debug('-'*80)
        self.log.debug('%s: serial system finished, time taken: %5.4fs', __name__, sysTime)
        self.log.debug('-'*80)
        return self.g.ReturnCode
    gaudimain.runSerial = runSerial
    # decorate runParallel to prevent it to run if a custom main loop is used
    gaudimain.oldRunParallel = gaudimain.runParallel
    def runParallel(self, ncpus):
        if self.mainLoop:
            self.log.fatal("cannot use custom main loop in multi-process mode")
            return 1
        return self.oldRunParallel(ncpus)
    gaudimain.runParallel = runParallel
    # main loop implementation, None stands for the default
    gaudimain.mainLoop = None
