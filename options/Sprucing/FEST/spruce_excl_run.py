"""An options file specifically to test sprucing+streaming for FEST

Run like any other options file:

    ./Moore/run gaudirun.py spruce_options.py hlt2_tck.py spruce_excl_run.py
"""
from Moore import options
from pprint import pprint

from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as bandq_lines
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as b2cc_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as sl_lines

##Should only need to change the following dictionary.
# Note that wg_lines are themselves dict types
linedict = {
    "b2oc": b2oc_lines,
    "bandq": bandq_lines,
    "b2cc": b2cc_lines,
    "sl": sl_lines
}
######################################################

lines_to_run = [
    item
    for sublist in [*{wg: [*linedict[wg].keys()]
                      for wg in linedict}.values()] for item in sublist
]

missing_lines = [
    item for item in list(sprucing_lines.keys()) if item not in lines_to_run
]

print("Lines to be run are ", lines_to_run)

print("The following lines exist but are not appended to any stream :",
      missing_lines, "\n end of missing lines.")


def make_streams():
    streamdict = {
        wg: [builder() for builder in linedict[wg].values()]
        for wg in linedict
    }

    pprint(streamdict)
    return streamdict


options.lines_maker = make_streams
