"""Options to dump passhtrough TCK.

Run like any other options file:

    ./Moore/run gaudirun.py spruce_options.py hlt2_tck.py spruce_passthro_run.py dump_passthro_tck.py

"""
from Gaudi.Configuration import appendPostConfigAction
from Moore.tcks import dump_passthrough_configuration


@appendPostConfigAction
def dump():
    dump_passthrough_configuration(None, "passthro_FEST.tck.json")
