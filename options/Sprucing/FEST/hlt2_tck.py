"""
An options file specifically to test sprucing+streaming for FEST

    ./Moore/run gaudirun.py spruce_options.py hlt2_tck.py spruce_{excl, passthro}_run.py

Temporary fix until tck infrastructure in place
"""
import os
from Moore.tcks import load_hlt2_configuration

load_hlt2_configuration(
    os.path.expandvars(
        "$APPCONFIGOPTS/Sprucing/FEST/hlt2_2or3bodytopo_realtime.tck.json"))
