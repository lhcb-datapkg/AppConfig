"""Options to dump sprucing TCK.

Run like any other options file:

    ./Moore/run gaudirun.py spruce_options.py hlt2_tck.py spruce_excl_run.py dump_sprucing_tck.py

"""
from Gaudi.Configuration import appendPostConfigAction
from Moore.tcks import dump_sprucing_configuration


@appendPostConfigAction
def dump():
    dump_sprucing_configuration(None, "spruce_FEST.tck.json")
