"""
An options file specifically to test sprucing+streaming for FEST

    ./Moore/run gaudirun.py spruce_options.py hlt2_tck.py spruce_{excl, passthro}_run.py
"""
from Moore import options
from Configurables import Moore

options.input_raw_format = 0.3
options.input_type = 'MDF'
options.simulation = True
options.data_type = 'Upgrade'

moore = Moore()
moore.spruce = True
moore.from_file = True
