### @file
#
#   Configures Moore to accept events with routing bit 55 (Hlt1 1Track) or bit 42 (DiMuon).
#
# /

from Configurables import Moore, HltRoutingBitsFilter
filter = HltRoutingBitsFilter( "RequireHltRoutingBit55OR42", RequireMask = [ 0, (0x1 << (42-32)) | (0x1 << (55-32)), 0 ] )
Moore().WriterRequires = [ filter.name() ]

# make sure filter is executed somewhere after the HLT sequence ...
from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr
appendPostConfigAction( lambda : ApplicationMgr().TopAlg.append( filter ) )
