### @file
#
#   Copy L0xHlt1 accepted events
#
#   This will produce a L0xHlt stripped DST, re-running the L0 
#   with config 0xff68, and Hlt config 0x3ff68 (see HltTCK for
#   more info on this configuration)
#
#   @author P. Koppenburg & G. Raven
#   @date 2009-11-02
# /
import Gaudi.Configuration
#
# Application
#
from Configurables import Moore
Moore().L0 = True
Moore().ReplaceL0BanksWithEmulated = True
Moore().UseTCK = True
Moore().InitialTCK = '0x0004ff68'
Moore().CheckOdin = False
Moore().WriterRequires = [ 'Hlt1Global' ]

# Dirac should set Moore().outputFile and Moore().inputFiles
# if no ':' in one of the input Files, the file is assumed to be
# a PFN, and it gets automatically prefixed by PFN; this does
# NOT happen for eg. castor://...... -- in those cases, the name
# is used 'as-is'.
#
#Moore().inputFiles = [ '/data/bfys/lhcb/MC/MC09/DST/00004985/0000/00004985_00000018_1.dst' ]
#Moore().outputFile = 'L0xHlt1.dst'

