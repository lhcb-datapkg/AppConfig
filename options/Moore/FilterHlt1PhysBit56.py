### @file
#
#   Configures Moore to accept events with routing bit 56 (Hlt1 1TrackAllL0).
#
# /

from Configurables import Moore, HltRoutingBitsFilter
filter = HltRoutingBitsFilter( "RequireHltRoutingBit56", RequireMask = [ 0, 0x1 << (56-32), 0 ] )
Moore().WriterRequires = [ filter.name() ]

# make sure filter is executed somewhere after the HLT sequence ...
from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr
appendPostConfigAction( lambda : ApplicationMgr().TopAlg.append( filter ) )
