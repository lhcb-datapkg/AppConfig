# Reassemble DAQ/RawEvent so that Moore is happy when running on input with a split raw event
from Configurables import Moore
Moore().EnableDataOnDemand = True

from Configurables import RecombineRawEvent
RecombineRawEvent().Version = 4.2
