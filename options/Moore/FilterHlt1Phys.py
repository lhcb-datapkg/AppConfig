### @file
#
#   Configures Moore to accept events with routing bit 46 (Hlt1 Physics).
#
#   ! 2011-04-14 - first version (tested on MC dst's)
#   ! 2011-04-18 - remove bank killer
#
#   @author Alexandr Kozlinskiy
#   @date 2011-04-14
# /

from Configurables import Moore, HltRoutingBitsFilter
filter = HltRoutingBitsFilter( "RequireHltRoutingBit46", RequireMask = [ 0, 0x1 << (46-32), 0 ] )
Moore().WriterRequires = [ filter.name() ]

# make sure filter is executed somewhere after the HLT sequence ...
from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr
appendPostConfigAction( lambda : ApplicationMgr().TopAlg.append( filter ) )
