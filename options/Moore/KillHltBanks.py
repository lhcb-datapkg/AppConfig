### @file
#
#   Kill all Hlt banks.
#
#   ! 2011-04-18 - code is provided by Gerhard
#
#   @author Gerhard Raven, Alexandr Kozlinskiy
#   @date 2011-04-18
# /

from Configurables import bankKiller
bk = bankKiller( "KillHltBanks", BankTypes = [ "HltRoutingBits", "HltSelReports", "HltVertexReports", "HltDecReports", "HltLumiSummary" ] )

# make sure killer is executed BEFORE the HLT sequence....
from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr
appendPostConfigAction( lambda : ApplicationMgr().TopAlg.insert( 0, bk ) )
