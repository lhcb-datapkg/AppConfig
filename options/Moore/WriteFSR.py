### @file
#
#   Set WriteFSR option to True in Moore.
#   This option specify if ouput should be produce even
#   in case of no output (for example due to trigger rejection)
#
#   ! 2011-07-08 - Alexandr Kozlinskiy
#
#   @author Alexandr Kozlinskiy
#   @date 2011-07-08
# /

from Configurables import Moore
Moore().WriteFSR = True
