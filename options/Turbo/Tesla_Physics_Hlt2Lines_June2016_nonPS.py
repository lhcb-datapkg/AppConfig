from __future__ import print_function
from Configurables import Tesla 
from Gaudi.Configuration import *
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict, prescaleDict

### 2016 basis
version='2016_0x21271600'
lines = streamLines(prodDict,version,'CharmHad',debug=True)
lines += streamLines(prodDict,version,'OniaBeautyStrange',debug=True)

### 2016 June additions
version_add = '2016_0x21331609_additions'
lines += streamLines(prodDict,version_add,'CharmHad_additions',debug=True)
lines += streamLines(prodDict,version_add,'OniaBeautyStrange_additions',debug=True)

### Find out the PS lines
prescaleVersion=4
prescaledLines=prescaleDict[prescaleVersion]["CharmHad"]

### Pick lines which are not prescaled
linesPS = list(prescaledLines.keys())
linesComplement=[]
for l in lines[:]:
    if l not in linesPS: linesComplement.append(l)
print("All lines has size: "+str(len(lines)))
print("Prescaled lines has size: "+str(len(linesPS)))
print("Complement of prescaled lines has size: "+str(len(linesComplement)))

Tesla().TriggerLines = linesComplement

# configuration for non-prescaled stream
Tesla().RemovePrescaleExcl=True
Tesla().NonPrescaleList=prescaleDict[prescaleVersion]["non-prescaled-PR"]
