from TurboStreamProd.streams import turbo_streams
from Configurables import Tesla

Tesla().EnableLineChecker = True
Tesla().IgnoredLines = [".*TurboCalib"]

print(turbo_streams.keys())
lines=[]
for vals in turbo_streams['2015'].values():
    lines+=vals['lines']

Tesla().TriggerLines = lines
