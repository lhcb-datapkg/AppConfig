from Configurables import Tesla 
Tesla().Pack = True
Tesla().InputType = "RAW"
Tesla().DataType = '2016'
Tesla().Simulation = False
Tesla().Mode = 'Online'
Tesla().RawFormatVersion = 0.2
Tesla().VertRepLoc = 'Hlt2'

### Already juggled online
Tesla().VetoJuggle = True
Tesla().KillInputHlt2Reps=True
Tesla().HDRFilter=True
