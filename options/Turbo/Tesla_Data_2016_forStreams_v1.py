from Configurables import Tesla 
Tesla().Pack = True
Tesla().InputType = "RAW"
Tesla().DataType = '2016'
Tesla().Simulation = False
Tesla().Mode = 'Online'
Tesla().RawFormatVersion = 0.2
Tesla().VertRepLoc = 'Hlt2'

Tesla().outputSuffix = ".mdst"
#Tesla().outputPrefix = "TURBO_"

### Already juggled online
Tesla().VetoJuggle = True

Tesla().HDRFilter=True
