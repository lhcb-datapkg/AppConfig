from Configurables import Tesla 
from Gaudi.Configuration import *

version='v9r9_0x004004c'
from TurboStreamProd.helpers import *
from TurboStreamProd import prodDict
lines = streamLines(prodDict,version,'PID',debug=True)
lines += streamLines(prodDict,version,'TrackCalib',debug=True)

Tesla().TriggerLines = lines
