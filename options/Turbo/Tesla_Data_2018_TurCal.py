"""Configuration for running Tesla over 2018 data processed by Brunel.

Requires at least one additional options file defining Tesla().TriggerLines or
Tesla().Streams.
"""
from Configurables import Tesla

Tesla().DataType = '2018'
Tesla().HDRFilter = True
Tesla().InputType = 'DST'
Tesla().Mode = 'Offline'
Tesla().Simulation = False
# Raw event format used for Brunel output
Tesla().SplitRawEventInput = 4.3
# Only one stream
Tesla().outputFile = 'FullTurbo.dst'
