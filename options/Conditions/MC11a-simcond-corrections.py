### Options to be used in DaVinci to pick up momentum scaling and smearing 
### for MC11a - Sim05 productions for 2011

from Configurables import CondDB
CondDB().LocalTags["SIMCOND"] = ["calib-20130325-1", "calib-20130426"]
