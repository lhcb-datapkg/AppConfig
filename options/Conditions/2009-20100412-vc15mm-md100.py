##############################################################################
# File for defining conditions with Velo closed at 15 mm and "Down" magnetic
# field with geometry as described on Jan 2010, in the same conditions as for
# 2009 data. As in 2009-Sim06Reco04{-withTruth}.
##############################################################################

from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20100407"
LHCbApp().CondDBtag = "MC-20100412-vc15mm-md100"

##############################################################################
