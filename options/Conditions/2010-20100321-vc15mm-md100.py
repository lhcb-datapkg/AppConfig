##############################################################################
# File for defining conditions with Velo closed and "Down" magnetic
# field with geometry and conditions as described in Feb 2010
# First version of tags for MC for 2010 data.
##############################################################################
from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20100119"
LHCbApp().CondDBtag = "sim-20100321-vc15mm-md100"

##############################################################################
