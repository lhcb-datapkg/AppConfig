#
# Overwrite at run time antideuteron name, which is wrong in DDDB tags for
# quite a while. The reason for this is that for the latest restrippings
# this has been fixed, but Sim09 productions still use older tags that have
# the wrong name and creating a new set of tags is not desirable we still
# need to support older strippings with the wrong name (which potentially
# might not matter) and also prefer not to touch Sim09 processing chain to
# support fully Gitlab DB. The properties correspond to the DDDB tag
# dddb-20170721-3.
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ "deuteron~             390 -1000010020  -1.0      1.87561300      0.000000e+00             anti-deuteron           0      0.00000000" ]

