from Gaudi.Configuration import *

importOptions("$APPCONFIGOPTS/Conditions/Velo-MC-2011-asInData.py")


from Configurables import UpdateManagerSvc

UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =0.451 ; double ResolPosLA = 0.451 ;  double ResolPosY = -0.02388 ;"]
