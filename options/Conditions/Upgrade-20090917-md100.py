##############################################################################
# File for defining Upgrade conditions with Velo closed and "Down" magnetic field
##############################################################################

from Configurables import LHCbApp

# this is needed since sets different DDDB file
LHCbApp().DataType  = "Upgrade"
# Minimal Upgrade Layout
#   this tag can be used to run Boole or Brunel also for data
#   generated with "mul-gauss-20090917" (aerogel out)
#   here aerogel is in
LHCbApp().DDDBtag   = "mul-20090917"
# same as for MC09
LHCbApp().CondDBtag = "sim-20090402-vc-md100"

##############################################################################
