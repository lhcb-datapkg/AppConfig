from Configurables import UpdateManagerSvc

UpdateManagerSvc().ConditionsOverride += [
    "Conditions/Online/LHCb/Magnet/Measured := double Current = 0.0",
    "Conditions/Online/LHCb/Magnet/Set := double Current = 0.0",
]
