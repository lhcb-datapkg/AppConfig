##############################################################################
# File for defining conditions with Velo closed and "Down" magnetic
# field with geometry and conditions as described in Feb 2010
# Tags for MC for 2010 data (2010-Sim03Reco03{-withTruth})
##############################################################################
from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20100407"
LHCbApp().CondDBtag = "sim-20100412-vc-md100"

##############################################################################
