##############################################################################
# File for running with FT_MonoLayer Upgrade configuration
##############################################################################
from Configurables import CondDB
CondDB().Upgrade = True

# Add the FT_MonoLayer tag
if "FT_StereoAngle4" not in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType += ["FT_StereoAngle4"]

# Remove the FT tag
if "FT" in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType.remove("FT")




