### 
### Override magnetic field maps to read maps v6.0, preliminary for Run3
from Configurables import UpdateManagerSvc

UpdateManagerSvc().ConditionsOverride +=  [
    "Conditions/HardwareProperties/LHCb/Magnet/FieldMapFilesUp := \
        string_v Files = field.v6r0.c1.up.cdf field.v6r0.c2.up.cdf field.v6r0.c3.up.cdf field.v6r0.c4.up.cdf",
    "Conditions/HardwareProperties/LHCb/Magnet/FieldMapFilesDown := \
        string_v Files = field.v6r0.c1.down.cdf field.v6r0.c2.down.cdf field.v6r0.c3.down.cdf field.v6r0.c4.down.cdf"
    ]



