'''Global configuration for Stripping29r2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29r2'
Stripping().TCK = 0x42722920
Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
