'''Global configuration for Stripping28r2p1.'''

from Configurables import Stripping

Stripping().Version = 'Stripping28r2p1'
Stripping().TCK = 0x44112821
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
