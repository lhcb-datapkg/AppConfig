'''Global configuration for Stripping29r2p2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29r2p2'
Stripping().TCK = 0x42112922
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
