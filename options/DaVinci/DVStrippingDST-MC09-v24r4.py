#
#   Stripping selections job (DST output)
#
#   @author J. Palacios/A. Poluektov
#   @date 2009-11-05
#

from Gaudi.Configuration import *
from Configurables import SelDSTWriter, DaVinci

from StrippingConf.Configuration import StrippingConf

from StrippingSelections import StreamBmuon, StreamHadron, StreamJpsi, StreamDstar, StreamLambda, StreamBelectron

sc = StrippingConf( Streams = [ StreamBmuon.stream
                    , StreamHadron.stream 
                    , StreamJpsi.stream
                    , StreamDstar.stream
                    , StreamLambda.stream
#                    , StreamBelectron.stream 
                    ] )

# Dirac should modify OutputFilePrefix.
# SelDSTWriter("StripMC09DSTWriter").OutputFileSuffix = '012345'
dstWriter = SelDSTWriter("StripMC09DSTWriter",
                         SelectionSequences = sc.activeStreams(),
                         OutputPrefix = 'Strip',
                         ExtraItems = ['/Event/DAQ/RawEvent']
                         )

DaVinci().DataType = "MC09"                   # Default is "MC09"
DaVinci().UserAlgorithms = [ dstWriter.sequence() ]

DaVinci().Hlt = True
DaVinci().HltThresholdSettings = 'Physics_320Vis_300L0_10Hlt1_Aug09'

DaVinci().L0 = True
DaVinci().ReplaceL0BanksWithEmulated = True

from Configurables import L0Conf
L0Conf().TCK = '0xFF68'

# DaVinci().EvtMax = 1000
