"""
Options for building Stripping20r1p3. 
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

stripping='stripping20r1p3'
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

stripTESPrefix = 'Strip'
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = stripTESPrefix )
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID' 
bhadronMicroDSTname    = 'Bhadron' 
dimuonNoRAWname        = 'Dimuon'
dimuonVELORAWname      = 'DimuonVELORAW'

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    bhadronMicroDSTname     : mdstElements
    }


SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking),

    dimuonNoRAWname          : stripDSTStreamConf(pack=enablePacking,
                                                  vetoItems = [ "/Event/Calo/RawEvent", 
                                                                "/Event/Rich/RawEvent",
                                                                "/Event/Other/RawEvent"]),
    dimuonVELORAWname        : stripDSTStreamConf(pack=enablePacking,
                                                  vetoItems = [ "/Event/Calo/RawEvent",
                                                                "/Event/Rich/RawEvent"]),

    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# MDST.DST Writer
# Streams to include in MDST.DST
mdstDstS = [ leptonicMicroDSTname, charmMicroDSTname, bhadronMicroDSTname ]
dstS = ['BhadronCompleteEvent','CharmCompleteEvent','Dimuon','DimuonVELORAW','EW','Semileptonic']
# Locations to veto from MDST.DST
tesVETO = [ "/Event/Rich/RawEvent",
            "/Event/Calo/RawEvent",
            "/Event/Muon/RawEvent",
            "/Event/Other/RawEvent" ]
tesVETO.extend([ '/Event/'+name+"#99" for name in dstS ])

from Configurables import InputCopyStream
from GaudiConf     import IOHelper
mdstDstWriter = InputCopyStream( "MDSTDSTWriter",
                                 AcceptAlgs = [ name+"_OStream" for name in mdstDstS ],
                                 OptItemList = [ '/Event/'+stripTESPrefix+'#99' ] + [ '/Event/'+name+"#99" for name in mdstDstS ],
                                 TESVetoList = tesVETO )
ioh_algs = IOHelper().outputAlgs( filename = '000000.MDST.dst',
                                  writer = mdstDstWriter,
                                  writeFSR = True )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32214213)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().appendToMainSequence( ioh_algs )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
