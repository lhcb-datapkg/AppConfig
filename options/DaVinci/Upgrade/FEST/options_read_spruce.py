###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for reading a spruced .dst file with DaVinci
"""
from DaVinci import options

options.annsvc_config = 'spruce_FEST.tck.json'
options.data_type = 'Upgrade'
options.enable_unpack = True
options.evt_pre_filters = {"TupleFilter": "HLT_PASS('SpruceB2OC_BdToDsmK_DsmToHHH_FEST_LineDecision')"}
options.histo_file = 'DaVinci_histos.root'
options.input_raw_format = 0.3
options.lumi = False
options.ntuple_file = 'DaVinci_tuple.root'
options.output_level = 3
options.print_freq = 100
options.simulation = True
options.unpack_stream = '/Event/Spruce'
options.user_algorithms = '$APPCONFIGROOT/options/DaVinci/Upgrade/FEST/algs_read_spruce:main'
