###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Algorithms for creating a tuple from a spruced .dst with DaVinci.
"""
from FunTuple import FunctorCollection
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple.NewTupleTools import Kinematics
from PyConf.application import make_data_with_FetchDataFromFile

bd2dsk_line = make_data_with_FetchDataFromFile(
    "/Event/Spruce/SpruceB2OC_BdToDsmK_DsmToHHH_FEST_Line/Particles")

branches_dsk = {
    'B0': "[B0 -> D_s- K+]CC",
    'Ds': "[B0 -> ^D_s- K+]CC",
    'Kp': "[B0 -> D_s- ^K+]CC"
}

variables = FunctorCollection({
    'LOKI_MAXPT': 'TRACK_MAX_PT',
    'LOKI_Muonp_PT': 'CHILD(PT, 1)',
    'LOKI_Muonm_PT': 'CHILD(PT, 2)',
})

variables_extra = FunctorCollection({
    'LOKI_NTRCKS_ABV_THRSHLD':
    'NINTREE(ISBASIC & (PT > 15*MeV))'
})
variables += variables_extra

#FunTuple: make functor collection from the imported functor library Kinematics
variables_all = FunctorCollection(Kinematics)

#FunTuple: associate functor collections to branch name
variables_dsk = {
    'ALL': variables_all,  #adds variables to all branches
    'B0': variables,
    'Ds': variables_extra,
    'Kp': variables_extra
}

loki_preamble = ['TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)']

tuple_B0DsK = Funtuple(
    name="B0DsK_Tuple",
    tree_name="DecayTree",
    branches=branches_dsk,
    variables=variables_dsk,
    loki_preamble=loki_preamble,
    inputs=bd2dsk_line)

def main():
    tools = []
    algs = {"B0DsK": [tuple_B0DsK]}

    return algs, tools
