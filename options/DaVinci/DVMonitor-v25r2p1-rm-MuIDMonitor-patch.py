##############################################################################
#
# Patch to kill MuIDMonitor in Full stream
#
# Author: Rob Lambert <rob.lambert@cern.ch>
#
##############################################################################
from DaVinci.Configuration import *
from Gaudi.Configuration import *


def rmMuIDPatch():
    #do for all types as in the name
    #inputType = DaVinci().getProp( "InputType" ).upper()
    #if ( inputType != "DST") :
    patchseq=[]
    kill1=["MuIDJpsiPlot", "MuIDLambdaPlot","MuonID2BodyLamSequence","MuonID2BodyJpsiSequence"]
    #print "######################################"
    #print DaVinci().MoniSequence
    for mem in DaVinci().MoniSequence:
        if mem.getName() not in kill1:
            patchseq.append(mem)
    DaVinci().MoniSequence=patchseq
    #print DaVinci().MoniSequence
    patchseq=[]
    for mem in GaudiSequencer("MonitoringSequence").Members:
        if mem.getName() not in kill1:
            patchseq.append(mem)
    GaudiSequencer("MonitoringSequence").Members=patchseq
    #print "######################################"

#def justPrintTheBloomingThing():
#    print DaVinci().MoniSequence
#    print ApplicationMgr().TopAlg
#    print GaudiSequencer("MonitoringSequence").Members
#    print [m.Members for m in ApplicationMgr().TopAlg if m.getName()=="MonitoringSequence"]

appendPostConfigAction(rmMuIDPatch)
#appendPostConfigAction(justPrintTheBloomingThing)
