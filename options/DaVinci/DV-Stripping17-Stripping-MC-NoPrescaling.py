"""
Options for building Stripping17 with strict ordering
of streams such that the micro-DSTs come last.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

streams.append( quickBuild('Dimuon') )
streams.append( quickBuild('Semileptonic') )
streams.append( quickBuild('Bhadron') )
streams.append( quickBuild('EW') )
streams.append( quickBuild('Radiative') )
streams.append( quickBuild('MiniBias') )

_calibration    = quickBuild('Calibration')  
_leptonic       = quickBuild('Leptonic')
_charm_complete = quickBuild('CharmCompleteEvent')
_charm_micro    = quickBuild('Charm')
_pid            = quickBuild('PID')


# Don't run TrackEff lines in MC
_calibration.lines[:] = [ x for x in _calibration.lines if 'TrackEff' not in x.name() ]

streams.append( _calibration )
streams.append( _charm_complete )
streams.append( _charm_micro )
streams.append( _leptonic )
streams.append( _pid )

#
# turn off all pre-scalings 
#
for stream in streams: 
    for line in stream.lines:
        line._prescale = 1.0 

#
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("AllStreams")

for stream in streams:
    AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000 )

AllStreams.sequence().IgnoreFilterPassed = True # so that we get all events written out

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2011"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().UseTrigRawEvent=True
