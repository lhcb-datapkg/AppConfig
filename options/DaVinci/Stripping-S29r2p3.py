'''Global configuration for Stripping29r2p3.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29r2p3'
Stripping().TCK = 0x46702923
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
