"""
Options for building Stripping19c, with track chi2/ndf<3 cut, to test data 2011 reprocessing with 2012 software
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping19c'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)

myconfig = {} 
myconfig['Beauty2Charm'] = config['Beauty2Charm']
myconfig['B2fourbody'] = config['B2fourbody']
myconfig['B2twobody'] = config ['B2twobody' ]
### myconfig['DstarD02Kpipi0'] = config['DstarD02Kpipi0']
myconfig['B2threebody' ] = config['B2threebody']

myconfig['Beauty2Charm']['CONFIG']['ALL']['TRCHI2DOF_MAX'] = 3.0
myconfig['Beauty2Charm']['CONFIG']['ALL']['MIPCHI2DV_MIN'] = 4.0 

#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = myconfig, archive = archive , WGs = 'B2OC' )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

# Switch off lines affected by the memory leak of DiElectronMaker
offList = [
    'StrippingChiCJPsiGammaConvChibLine',
    'StrippingChiCJPsiGammaConvChibSymLine',
    'StrippingChiCJPsiGammaConvChicLine',
    'StrippingChiCJPsiGammaConvChicSymLine',
    'StrippingBu2LLK_eeLine2',
    'StrippingBd2eeKstarBDTLine2'
    ]

# Charm control stream, for 10% of events from CharmToBeSwum
for stream in streams : 
    if stream.name() == 'CharmToBeSwum' : charmToBeSwumStream = stream
    for line in stream.lines:
        if line.name() in ['StrippingMBNoBias']:
            line._prescale = 0.1
        if line.name() in offList:
            line._prescale = 0.

## _CharmControl = StrippingStream("CharmControl")
## _CharmControl.appendLines( cloneLinesFromStream( charmToBeSwumStream, 'CharmControl', prescale = 0.1 ) )

## streams.append( _CharmControl )

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = 'Strip' )

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf
                                      )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID' 
### bhadronMicroDSTname    = 'Bhadron' 

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    pidMicroDSTname         : mdstElements,
    ### bhadronMicroDSTname     : mdstElements
    }


SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    ### bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )


# Add cone variables to Bhadron stream
from Configurables import AddExtraInfo, ConeVariables, ConeVariablesForEW

bhadronOutputLocations = []

for stream in streams :
    if stream.name() == 'Bhadron' : bhadronOutputLocations = [ "/Event/" + x for x in stream.outputLocations() ]

bhadron_extra = AddExtraInfo('BhadronExtraInfo')
bhadron_extra.Inputs = bhadronOutputLocations

cv1   = ConeVariables('BhadronExtraInfo.ConeVariables1', ConeAngle = 1.5, ConeNumber = 1,
                       Variables = ['angle', 'mult', 'ptasy'] )
cv2   = ConeVariables('BhadronExtraInfo.ConeVariables2', ConeAngle = 1.7, ConeNumber = 2,
                       Variables = ['angle', 'mult', 'ptasy'] )
bhadron_extra.addTool( cv1 , 'BhadronExtraInfo.ConeVariables1')
bhadron_extra.addTool( cv2 , 'BhadronExtraInfo.ConeVariables2')
bhadron_extra.Tools = [ 'ConeVariables/BhadronExtraInfo.ConeVariables1',
                        'ConeVariables/BhadronExtraInfo.ConeVariables2' ]

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x3210019c)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ bhadron_extra ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

