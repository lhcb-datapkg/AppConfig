##############################################################################
# $Id: DVMonitorDst-v24r3Patch.py,v 1.1 2009-10-05 16:27:11 pkoppenb Exp $
#
# syntax: gaudirun.py $DAVINCIMONITORSROOT/options/DVMonitorDst.py
#
# Author: Patrick Koppenburg <patrick.koppenburg@cern.ch>
#
##############################################################################
from DaVinci.Configuration import *
from Gaudi.Configuration import *
##############################################################################
#
# the stuff
#
from Configurables import EventCountHisto
DaVinci().MoniSequence += [ EventCountHisto("DaVinciMonitor") ] 
importOptions( "$DAVINCIMONITORSROOT/options/Jpsi2MuPi.py" ) 
importOptions( "$DAVINCIMONITORSROOT/options/RichCalib.py" ) 
#importOptions( "$DAVINCIMONITORSROOT/options/MuonPidJpCalib.py" ) 
#importOptions( "$DAVINCIMONITORSROOT/options/MuonPidLaCalib.py" ) 
#importOptions( "$DAVINCIMONITORSROOT/options/MuID2BodyPlot.py" )
##############################################################################
# Hack for v24r3
def noRecal():
    from Configurables import GaudiSequencer
    GaudiSequencer("ProtoPRecalibration").Members = []

appendPostConfigAction(noRecal)
##############################################################################
#
# Histograms
#
DaVinci().HistogramFile = "DVMonitors.root"
##############################################################################
#
# Most of this will be configured from Dirac
#
##############################################################################
DaVinci().EvtMax = -1
DaVinci().DataType = "2008" # Default is "DC06"
DaVinci().Simulation = True

