"""
Options for building Stripping22. 
"""

#use CommonParticlesArchive
stripping='stripping22'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)


from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
tmpconfig  = strippingConfiguration(stripping)

config={}

from copy import deepcopy

#change GEC for B2DMuNuX
for k,i in tmpconfig.iteritems():
    config[k]=i
    if k=='B2DMuNuX':
        ((config[k])['CONFIG'])['GEC_nLongTrk']=100000

#get the line builders from the archive
archive = strippingArchive(stripping)

stream = buildStream(stripping = config, streamName='ALL', archive = archive)

stream.lines=[l for l in stream.lines if (("TrackEffDown" not in l.name()))] #fix for Z02mumu bug

#add line for ridge
from StrippingConf.StrippingLine import StrippingLine
rl=StrippingLine( "HighVeloMultLine",
                   prescale=1.0,
                   checkPV=False,
                   HLT1 = "HLT_PASS_RE('^Hlt1(HighVeloMult|HighVeloMultSinglePV)Decision$')",
                   HLT2 = "HLT_PASS_RE('Hlt2PassThroughDecision')")
stream.appendLines([rl])


from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [stream],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = 'Strip' )

enablePacking = True

from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }


SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True) }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x38002200)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
