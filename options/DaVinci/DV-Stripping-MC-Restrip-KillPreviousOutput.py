# Kill the output of a previous stripping step so data can be
# restripped.

from Configurables import EventNodeKiller, DaVinci

eventNodeKiller = EventNodeKiller('StripKiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
# Kill old stripping banks first
DaVinci().prependToMainSequence( [ eventNodeKiller ] )
