'''Global configuration for Stripping29r1.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29r1'
Stripping().TCK = 0x42612910
Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
