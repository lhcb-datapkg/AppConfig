'''Global configuration for Stripping29r2p1.'''

from Configurables import Stripping

Stripping().Version = 'Stripping29r2p1'
Stripping().TCK = 0x42922921
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
