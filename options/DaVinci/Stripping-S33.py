'''Global configuration for Stripping33.'''

from Configurables import Stripping, TrackSys

TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

Stripping().Version = 'Stripping33'
Stripping().TCK = 0x42743300
Stripping().MaxCandidates = 2000
