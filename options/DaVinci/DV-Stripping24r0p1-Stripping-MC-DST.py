"""
Options for building Stripping24r0p1. 
"""

#use CommonParticlesArchive
stripping='stripping24r0p1'
from CommonParticlesArchive import CommonParticlesArchiveConf
from functools import reduce
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = dict(strippingConfiguration(stripping))

# !!! BUGFIX: Last minute inclusion of MiniBias, just use same config as S28.
config28 = strippingConfiguration("stripping28")
config["MiniBias"] = config28["MiniBias"]

#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("AllStreams")

# These lines are needed to remove duplicates (everything goes to the same stream)
for stream in streams:
    if 'MiniBias' not in stream.name():
        for line in stream.lines:
            dup=False
            for line2 in AllStreams.lines:
                if line2.name()==line.name():
                    dup=True
                    break
            if not dup:
                AllStreams.appendLines([line]) 

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = True

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x38142401)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# !!! BUGFIX: Fix related info output location for RareStrange
line = filter(lambda line : 'RareStrangePhiKMu' in line.name(), reduce(lambda x, y : x + y, (stream.lines for stream in streams), []))[0]
for relconf in line.RelatedInfoTools :
    relconf['Location'] = relconf['Location'].replace('K', 'Phi')

# !!! BUGFIX: Fix input location for StrippingXB2DPiP
from Configurables import LoKi__VoidFilter, FilterDesktop
voidfilter = LoKi__VoidFilter('SelFilterPhys_StdLooseDplusKKPi_Particles')
voidfilter.Code = voidfilter.Code.replace('StdLooseDplusKKPi', 'StdLooseDplus2KKPi')

filterdesktop = FilterDesktop('XB2DPiPSelDpDs')
filterdesktop.Inputs.remove('Phys/StdLooseDplusKKPi/Particles')
filterdesktop.Inputs.append('Phys/StdLooseDplus2KKPi/Particles')

