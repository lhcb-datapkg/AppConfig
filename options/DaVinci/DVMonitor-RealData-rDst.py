##############################################################################
# $Id: DVMonitor-RealData-rDst.py,v 1.1 2009-11-23 13:47:40 pkoppenb Exp $
#
# Author: Patrick Koppenburg <patrick.koppenburg@cern.ch>
#
##############################################################################
from DaVinci.Configuration import *
from Gaudi.Configuration import *
##############################################################################
#
# the stuff
#
importOptions( "$DAVINCIMONITORSROOT/options/DaVinciMonitoring.py") 
##############################################################################
#
# Histograms
#
DaVinci().HistogramFile = "DVMonitors.root"
##############################################################################
#
# Most of this will be configured from Dirac
#
##############################################################################
DaVinci().EvtMax = -1
DaVinci().InputType = "RDST"
DaVinci().DataType = "2009" 
DaVinci().Simulation = False
