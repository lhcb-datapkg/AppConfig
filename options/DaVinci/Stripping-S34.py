'''Global configuration for Stripping34.'''

from Configurables import Stripping

Stripping().Version = 'Stripping34'
Stripping().TCK = 0x44403400
Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
