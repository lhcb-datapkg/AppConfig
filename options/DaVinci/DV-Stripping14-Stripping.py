"""
Options for building Stripping 14 for the
2010 re-processing.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingSelections.Utils import buildStreams

config  = strippingConfiguration('stripping14')
streams = buildStreams(config)

leptonicMicroDSTname = 'Leptonic'
charmMicroDSTname    = 'Charm'
charmCopyDSTname     = 'CharmCompleteEvent'
calibrationFullDSTname = 'Calibration'



from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements)
#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements = stripMicroDSTElements()

#
# Configuration of Calibration stream 
# to include additional track containers
#
copyExtraTrackData = MoveObjects(objects = [ "Rec/TrackEffMuonTT_SelMuonTTPParts/ProtoParticles",
                                             "Rec/TrackEffMuonTT_SelMakeMuonTT/Tracks",
					     "Rec/Downstream/Tracks",
					     "Rec/Downstream/FittedTracks",
					     "Rec/ProtoP/DownMuonTrackEffDownMuonNominalProtoPMaker/ProtoParticles",
					     "Rec/VeloMuon/Tracks",
					     "Rec/ProtoP/VeloMuonTrackEffVeloMuonProtoPMaker/ProtoParticles" ])
				 

calibElements = stripDSTElements() + [ copyExtraTrackData ]

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(),
    charmMicroDSTname      : mdstElements,
    leptonicMicroDSTname   : mdstElements,
    calibrationFullDSTname : calibElements
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(),
    charmMicroDSTname      : mdstStreamConf,
    leptonicMicroDSTname   : mdstStreamConf,
    calibrationFullDSTname : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType     = 'SDST'
DaVinci().DataType      = "2010"
DaVinci().EvtMax        = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )


