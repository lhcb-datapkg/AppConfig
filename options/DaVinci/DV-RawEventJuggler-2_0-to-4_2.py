#
# Raw event juggler to split RawEvent
#
from Configurables import GaudiSequencer, RawEventJuggler
jseq=GaudiSequencer("RawEventSplitSeq")
juggler=RawEventJuggler("rdstJuggler")
juggler.Sequencer=jseq
juggler.Input=2.0  # 2012 Brunel format 
juggler.Output=4.2 # Reco15 format

from Configurables import DaVinci
DaVinci().prependToMainSequence( [jseq] )
