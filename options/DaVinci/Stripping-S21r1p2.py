'''Global configuration for Stripping21r1p2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping21r1p2'
Stripping().TCK = 0x39162112
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
