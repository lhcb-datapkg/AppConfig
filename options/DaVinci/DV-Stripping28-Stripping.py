"""
Options for building Stripping28. 
"""

#use CommonParticlesArchive
stripping='stripping28'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)


from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)
## Remove the MCNoPID lines from B2OC
for stream in streams:
  stream.lines=[line for line in stream.lines if "MCwNoPID" not in line.name()]

leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID'
bhadronMicroDSTname    = 'Bhadron'
mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent",  "Dimuon",
                "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck, GaudiSequencer

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    ActiveMDSTStream = True,
                    TESPrefix = stripTESPrefix,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )

#from Configurables import StrippingReport
#sr = StrippingReport(Selections = sc.selections())
#sr.ReportFrequency = 1000

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf)

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
mdstElements   = stripMicroDSTElements(pack=enablePacking)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    charmMicroDSTname       : mdstElements,
    leptonicMicroDSTname    : mdstElements,
    bhadronMicroDSTname     : mdstElements,
    pidMicroDSTname         : mdstElements
    }


SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True),
    charmMicroDSTname        : mdstStreamConf,
    leptonicMicroDSTname     : mdstStreamConf,
    bhadronMicroDSTname      : mdstStreamConf,
    pidMicroDSTname          : stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41402800)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1# Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
#DaVinci().appendToMainSequence( [ sr ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

##### FIX FOR LAST MINUTE CHANGES IN S28. SHOULD BE REMOVED IF NOT S28 #####
for stream in streams:
  for line in stream.lines:
    if 'B02DsstarK' in line.name():
      from Configurables import GaudiSequencer, CombineParticles
      gc = GaudiSequencer("StrippingB02DsstarKDsstar2DGammaD2HHHBeauty2CharmLineFilterSequence")
      cb = CombineParticles("B02DsstarKDsstar2DGammaD2HHHBeauty2Charm")
      cb.setProp('DecayDescriptors',["B0 -> D*_s- K+","B0 -> D*_s+ K-"])

# Change the column size of Timing table
from Configurables import AuditorSvc, TimingAuditor, SequencerTimerTool, NameAuditor
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
