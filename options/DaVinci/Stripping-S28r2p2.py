'''Global configuration for Stripping28r2p2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping28r2p2'
Stripping().TCK = 0x46702822
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
