'''Global configuration for Stripping34r0p2.'''

from Configurables import Stripping

Stripping().Version = 'Stripping34r0p2'
Stripping().TCK = 0x44113402
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
