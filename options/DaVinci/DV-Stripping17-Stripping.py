"""
Options for building Stripping17 with strict ordering
of streams such that the micro-DSTs come last.
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping17'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

streams.append( quickBuild('Dimuon') )
streams.append( quickBuild('Semileptonic') )
streams.append( quickBuild('Bhadron') )
streams.append( quickBuild('EW') )
streams.append( quickBuild('Radiative') )
streams.append( quickBuild('MiniBias') )
streams.append( quickBuild('Calibration') )  

_leptonic       = quickBuild('Leptonic')
_charm_complete = quickBuild('CharmCompleteEvent')
_charm_micro    = quickBuild('Charm')
_pid            = quickBuild('PID') 

streams.append( _charm_complete )
streams.append( _charm_micro )
streams.append( _leptonic )
streams.append( _pid )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements,
                                              stripCalibMicroDSTStreamConf
                                              )

#
# Configuration of MicroDST
#
mdstStreamConf = stripMicroDSTStreamConf()
mdstElements   = stripMicroDSTElements()

#
# Configuration of Calibration stream 
# to include additional track containers
#
copyExtraTrackData = MoveObjects(objects = [ "Rec/TrackEffMuonTT_SelMuonTTPParts/ProtoParticles",
                                             "Rec/TrackEffMuonTT_SelMakeMuonTT/Tracks",
                                             "Rec/Downstream/Tracks",
                                             "Rec/Downstream/FittedTracks",
                                             "Rec/ProtoP/DownMuonTrackEffDownMuonNominalProtoPMaker/ProtoParticles",
                                             "Rec/VeloMuon/Tracks",
                                             "Rec/ProtoP/VeloMuonTrackEffVeloMuonProtoPMaker/ProtoParticles" ])                          

calibElements = stripDSTElements() + [ copyExtraTrackData ]


calibrationFullDSTname = 'Calibration'
leptonicMicroDSTname   = 'Leptonic'
charmMicroDSTname      = 'Charm'
pidMicroDSTname        = 'PID' 

#
# Configuration of SelDSTWriter
#
SelDSTWriterElements = {
    'default'              : stripDSTElements(),
    calibrationFullDSTname : calibElements,
    charmMicroDSTname      : mdstElements,
    leptonicMicroDSTname   : mdstElements,
    pidMicroDSTname        : mdstElements
    }


SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(),
    charmMicroDSTname      : mdstStreamConf,
    leptonicMicroDSTname   : mdstStreamConf,
    pidMicroDSTname        : stripCalibMicroDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'SDST'
DaVinci().DataType = "2011"
DaVinci().EvtMax = 1000                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().UseTrigRawEvent=True
