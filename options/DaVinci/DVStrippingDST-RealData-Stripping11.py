from Gaudi.Configuration import *

from Configurables import DaVinci

from StrippingConf.Configuration import StrippingConf

from StrippingSettings.Stripping11 import StreamCalibration
from StrippingSettings.Stripping11 import StreamBhadron
from StrippingSettings.Stripping11 import StreamCharm
from StrippingSettings.Stripping11 import StreamDielectron
from StrippingSettings.Stripping11 import StreamDimuon
from StrippingSettings.Stripping11 import StreamMiniBias
from StrippingSettings.Stripping11 import StreamSemileptonic
from StrippingSettings.Stripping11 import StreamRadiative
from StrippingSettings.Stripping11 import StreamEW

from StrippingSettings.Stripping11 import StreamCharmMicroDST
from StrippingSettings.Stripping11 import StreamLeptonicMicroDST


# Create new streams for Charm 


streamBhadron =  StreamBhadron.stream
streamCharm   =  StreamCharm.stream

from StrippingSelections.StrippingB2twobody_prompt import B2twobody_promptLine
streamBhadron.appendLines( B2twobody_promptLine() ) 

from StrippingSelections.StrippingD2hhh_conf import StrippingD2hhhConf
streamCharm.appendLines( [
    StrippingD2hhhConf().stripDs2PPP() ,
    StrippingD2hhhConf().stripDs2KKP() ,
    StrippingD2hhhConf().stripDs2KPPos()
    ] )


# Now need a hack to rename the streams
# First, rename the "LeptonicMicroDST" stream to "Leptonic"
# Files will then end with "leptonic.mdst"
from StrippingConf.StrippingStream import StrippingStream
streamLeptonic = StrippingStream("Leptonic")
streamLeptonic.appendLines( StreamLeptonicMicroDST.stream.lines )

# Now create a new stream for the full charm, called "CharmControl"
# Files will end with "charmcontrol.dst"
streamCharmControl = StrippingStream("CharmControl")
streamCharmControl.appendLines( StreamCharm.stream.lines )

# Finally, rename the "CharmMicroDST" stream to "Charm"
# Files will end with "charm.mdst"
streamCharmNew = StrippingStream("Charm")
streamCharmNew.appendLines( StreamCharmMicroDST.stream.lines )


allStreams = [
    StreamCalibration.stream, 
    streamBhadron,
    streamCharmControl,
    StreamDielectron.stream,
    StreamDimuon.stream,
    StreamMiniBias.stream,
    StreamSemileptonic.stream,
    StreamRadiative.stream,
    StreamEW.stream,
    streamCharmNew,
    streamLeptonic
    ]


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

sc = StrippingConf( Streams = allStreams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

from DSTWriters.__dev__.microdstelements import *
from DSTWriters.__dev__.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              microDSTStreamConf,
                                              microDSTElements)

#
# Configuration of MicroDST
#

mdstStreamConf = microDSTStreamConf()
mdstStreamConf.extraItems += [ '/Event/Trig/L0/L0DUReport#1' ,
                               '/Event/Trig/L0/MuonBCSU#1'   ,
                               '/Event/Trig/L0/FullCalo#1'  ,
                               '/Event/Rec/Status#1',
                               '/Event/Strip/Phys/DecReports#1' ]

mdstElements = [
    CloneRecHeader(),
    CloneODIN(),
    ClonePVs(copyTracks = True),
    CloneParticleTrees(copyProtoParticles = True),
    ClonePVRelations("Particle2VertexRelations",True),
    CloneLHCbIDs() ,
    ReFitAndClonePVs() ,
    CloneRawBanks( banks = [ 'HltSelReports' , 'HltDecReports'   ] ) 
   ]



#
# Configuration of Calibration stream 
# to include additional track containers
#

copyExtraTrackData = MoveObjects(objects = [ "Rec/Track/MuonTTTracks",
                                             "Rec/ProtoP/MuonTTProtoP" ] )


calibElements = stripDSTElements() + [ copyExtraTrackData ]

#
# Configuration of SelDSTWriter
#



charmStreamName    = streamCharmNew.name()
leptonicStreamName = streamLeptonic.name()
calibStreamName    = StreamCalibration.stream.name()

SelDSTWriterElements = {
    'default'          : stripDSTElements(),
    charmStreamName    : mdstElements,
    leptonicStreamName : mdstElements,
    calibStreamName    : calibElements
    }


SelDSTWriterConf = {
    'default'            : stripDSTStreamConf(),
    charmStreamName      : mdstStreamConf,
    leptonicStreamName   : mdstStreamConf,
    calibStreamName      : stripDSTStreamConf()
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

#
# DaVinci Configuration
#



DaVinci().EvtMax = 1000                        # Number of events
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
EventSelector().Input   = [ "   DATAFILE='~/w0/Brunel.dst' TYP='POOL_ROOTTREE' OPT='READ'" ]

