'''Configure the Stripping for MC. Output will be a single DST stream 
called AllStreams.dst and 'MC only' lines will run.'''

from Configurables import Stripping

Stripping().Simulation = True
