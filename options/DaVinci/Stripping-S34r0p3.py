'''Global configuration for Stripping34r0p3.'''

from Configurables import Stripping

Stripping().Version = 'Stripping34r0p3'
Stripping().TCK = 0x46703403
#Stripping().MaxCombinations = 10000000
Stripping().MaxCandidates = 2000
