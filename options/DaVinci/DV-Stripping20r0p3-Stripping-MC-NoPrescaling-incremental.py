"""
Options for building Stripping20r0p2,
with tight track chi2 cut (<3)
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20r0p3'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

#
# Remove all prescalings 
# Merge into one stream and run in flag mode
#
AllStreams = StrippingStream("AllStreams3")

for stream in streams:
    for line in stream.lines:
        line._prescale = 1.0
    if 'MiniBias' not in stream.name():
        AllStreams.appendLines(stream.lines)

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip3'
                    )

AllStreams.sequence().IgnoreFilterPassed = True # so that we get all events written out

#
# Configuration of SelDSTWriter
#
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

stripPrefixes = ['Strip', 'Strip1', 'Strip2', 'Strip3']

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking, stripPrefix = stripPrefixes)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking, stripPrefix = stripPrefixes)
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip3/Phys/DecReports', TCK=0x32214203)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60



# For tests
ApplicationMgr().ExtSvc += [ 'AuditorSvc' ]
#AuditorSvc().Auditors += [ 'MemoryAuditor' ]
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )
#from Configurables import StrippingReport
#sr = StrippingReport(Selections = sc.selections(), OnlyPositive = False, EveryEvent = False)
#DaVinci().appendToMainSequence( [ sr ] )
