# Suppresses warnings from DST writers in Stripping25 due to missing ST clusters.
# This was due to the output format of Brunel being 4.1 rather than 4.2, so 
# the ST error banks aren't saved, and DaVinci can't recover ST clusters from
# the error banks in the same way as Brunel.

import Gaudi.Configuration

def silence_dst_writers() :
    clusterwriters = filter(lambda name : 'PackTkClusters' in name, Gaudi.Configuration.allConfigurables)
    for writer in clusterwriters :
        Gaudi.Configuration.allConfigurables[writer].OutputLevel = 5

Gaudi.Configuration.appendPostConfigAction(silence_dst_writers)
