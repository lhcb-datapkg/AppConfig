###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Helper functions, which will translate the parameters and tupleTools from the config.yaml into a DecayTreeTuple.
parse_tools() is a helper function to parse the tools for a configurable.
configure_dtt() will create a DecayTreeTuple instance from the configuration
and calls parse_tools() to add the tools and parse the parameters.
"""
from Configurables import DaVinci, DecayTreeTuple, MCDecayTreeTuple
from DecayTreeTuple.Configuration import addTupleTool

try:
    from six import string_types
except ImportError:
    string_types = (str, unicode)

def parse_tools(configurable, tools, verbose = False):
    configurable_name = configurable.name()
    for tool in tools:
        if isinstance(tool, string_types):
            configurable.ToolList += [tool]
            if verbose: print("Adding {tool} to {configurable_name}.ToolList".format(**locals()))
        elif isinstance(tool, dict):
            assert(len(tool) == 1) # must have one key: the name
            name = [key for key in tool][0]
            if verbose: print("Adding {name} to {configurable_name}".format(**locals()))
            tool_conf = configurable.addTupleTool(name)
            tool_conf_name = tool_conf.name()
            for option, value in tool[name].items():
                if verbose: print("	{tool_conf_name}.{option} = {value}".format(**locals()))
                tool_conf.setProp(option, value)

def configure_dtt(config, verbose = False):
    dtt_class, dtt_name = config["name"].split("/")
    if dtt_class == "DecayTreeTuple":
        dtt = DecayTreeTuple(dtt_name)
    elif dtt_class == "MCDecayTreeTuple":
        dtt = MCDecayTreeTuple(dtt_name)
    else:
        raise ValueError("Class "+dtt_class+" not recognised")
    dtt.setDescriptorTemplate(config["descriptorTemplate"])
    dtt.Inputs = config["inputs"]
    if DaVinci().InputType == "MDST":
        dtt.Inputs = [i.removeprefix(DaVinci().RootInTES+"/") for i in dtt.Inputs]
    dtt.ToolList = [] # Usually constructed with a default ToolList, but we want to replace that
    if "tools" in config:
        parse_tools(dtt, config["tools"], verbose)
    if "branches" in config:
        for particle in config["branches"]:
            if "tools" in config["branches"][particle]:
                parse_tools(getattr(dtt, particle), config["branches"][particle]["tools"], verbose)
    if "groups" in config:
        for group in config["groups"]:
            for particle in group.split(","):
                if "tools" in config["groups"][group]:
                    parse_tools(getattr(dtt, particle), config["groups"][group]["tools"], verbose)
    if verbose:
        print(dtt)
        for particle in config["branches"]:
            print(getattr(dtt, particle))
    return dtt

def save_config(dtt, verbose = False):
    """
        Need to capture all non-blank properties and those of all child configurables
    """
    raise NotImplementedError()

