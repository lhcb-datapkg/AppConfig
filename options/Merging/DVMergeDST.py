from Gaudi.Configuration import *
from Configurables import (DaVinci, LumiAlgsConf)

DaVinci().InputType = 'MDST'
DaVinci().UserAlgorithms=[InputCopyStream()]

######################### CAUTION ######################
# Seriously, this next line is for production jobs only
# Don't cut and paste it for anything else please!
LumiAlgsConf().SetFSRStatus="VERIFIED"
# Thanks!
########################################################
