from Gaudi.Configuration import *
from Configurables import DaVinci, LumiAlgsConf

DaVinci().InputType = 'MDST'
DaVinci().DataType = '2010'
DaVinci().UserAlgorithms = [ InputCopyStream() ]

######################### CAUTION ######################
# Seriously, this next line is for production jobs only
# Don't cut and paste it for anything else please!
LumiAlgsConf().SetFSRStatus = "VERIFIED"
# Thanks!
########################################################

#Merge all low-level FSRs into a single top-level FSR
#This can save 75% of the file size of small files,
#and it prevents the memory explosion at the end of user jobs
if 'MergeFSR' in DaVinci.__slots__:
    DaVinci().MergeFSR = True
elif 'MergeFSR' in LumiAlgsConf.__slots__:
    LumiAlgsConf().MergeFSR = True

#
# A change in the IODataManager().AgeLimit in the DaVinci configurable
# has made the FSR Conv Service segfault at the end of the job
#
# This is exposed whenever an output file is generated from more than one
# input file, with FSRs required on the output file.
#
# See bug #75795 for details
#
from Gaudi.Configuration import *
IODataManager().AgeLimit=2
#
# 2 is the default from the C++ class.
#

