#
#   Stripping tagger
#
#   Runs on merged DSTs and createsEvent tag collection file  
#
#   @author P. Koppenburg
#   @date 2010-04-28
#


from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from Configurables import EventTuple, TupleToolEventInfo  #, TupleToolTrigger
tag = EventTuple("TagCreator")
tag.EvtColsProduce = True
tag.ToolList = [ "TupleToolEventInfo",
                 "TupleToolRecoStats",
                 "TupleToolStripping",
#                 "TupleToolPrimaries"   # not compatible with ETC ? 
                  ] # , "TupleToolTrigger"
tag.addTool(TupleToolEventInfo)
tag.TupleToolEventInfo.Verbose=True
# tag.addTool(TupleToolTrigger)
# tag.TupleToolTrigger.VerboseL0=True
# tag.TupleToolTrigger.VerboseHlt1=True
# tag.TupleToolTrigger.UseAutomaticTriggerList = True

from Configurables import ProcStatusCheck

seqTag = GaudiSequencer("TagSequencer")
seqTag.ModeOR = True

seqTag.Members = [ ProcStatusCheck(), tag ]

from Configurables import DaVinci

DaVinci().InputType = 'MDST'

DaVinci().MoniSequence = [ seqTag ] 
DaVinci().PrintFreq = 100

DaVinci().EvtMax = -1                        # Number of events
DaVinci().ETCFile = "ETC.root"               # To be set by Dirac

# gaudirun.py $APPCONFIGROOT/options/Merging/DVTagger.py $APPCONFIGROOT/options/DaVinci/DataType-2010.py $APPCONFIGROOT/options/UseOracle.py
