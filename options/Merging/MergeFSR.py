#Merge all low-level FSRs into a single top-level FSR
#This can save 75% of the file size of small files,
#and it prevents the memory explosion at the end of user jobs

from Configurables import DaVinci, LumiAlgsConf

if 'MergeFSR' in DaVinci.__slots__:
    DaVinci().MergeFSR=True
elif 'MergeFSR' in LumiAlgsConf.__slots__:
    LumiAlgsConf().MergeFSR=True
