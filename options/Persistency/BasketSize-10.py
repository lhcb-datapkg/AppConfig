## Options needed to keep memory in Gauss under control
from Configurables import RootCnvSvc
RootCnvSvc(ApproxEventsPerBasket=10)
