# Improved compression for merged stripping output.

from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "LZMA:4"
