#!/usr/bin/env python
# =============================================================================
# $Id: KaliPi0_producefmDST.py$ 
# =============================================================================
## @file
#  The basic configuration for fmDST production
#  @author Daria Savrina Daria.Savrina@cern.ch
#  @date   2010-10-30
# =============================================================================
"""
The basic configuration for fmDST production for iterative pi0-calibration of
the ECAL. SDSTs are used as input.

Usage:

  > gaudirun.py KaliPi0_producefmDST.py

Or (suitable for tests)

  > python KaliPi0_producefmDST.py

"""
from __future__ import print_function
# =============================================================================
__author__  = " Daria Savrina Daria.Savrina@cern.ch "
__date__    = " 2009-10-30 "
__version__ = " CVS Tag $Name: not supported by cvs2svn $, version $Revision: $"
# =============================================================================
## ============= the basic import ====================
from   Gaudi.Configuration       import *
from   GaudiKernel.SystemOfUnits import MeV 

## =============== configuring Kali fmDST production =================
from KaliCalo.Configuration import firstPass

kali = firstPass (
    ##-- cuts and filters
    PtGamma           = 250 * MeV ,  ## selection cuts
    ## event multiplicity filter
    Filter            = "(650. > CONTAINS('Raw/Spd/Digits')) & ( CONTAINS('Rec/Vertex/Primary') > 0.) & ( CONTAINS('Rec/Vertex/Primary') < 5.)",

    ##-- output
    NTupleProduce    = False ,                          # do not produce NTuples
    Histograms       = False ,                          # do not produce Histograms
    FemtoDST         = "KaliPi0_Real2012.fmDST",        # output fmDST name

    ##-- general (configuring DaVinci)
    DataType         = '2012',                          # data type
    EvtMax           =  -1,                          # number of events to run
    OutputLevel      = ERROR
    )

## ======== input data type - 'SDST' ============
from Configurables import DaVinci
DaVinci().InputType = 'SDST'                            # use SDSTs as an input

if '__main__' == __name__ :

    ## make printout of the own documentations
    print('*'*120)
    print(__doc__)
    print(' Author  : %s ' %   __author__)
    print(' Version : %s ' %   __version__)
    print(' Date    : %s ' %   __date__)
    print('*'*120)

    from GaudiPython.Bindings import AppMgr
    gaudi = AppMgr()
    gaudi.run(-1)

## THE END
