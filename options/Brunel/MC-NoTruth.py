##############################################################################
# File for running Brunel on MC data, reading and saving no MC Truth,
# except for pileup information. DataType should be set separately
#
# Syntax is:
# gaudirun.py Brunel/MC-NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Brunel

Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().DigiType  = "Minimal"

##############################################################################
