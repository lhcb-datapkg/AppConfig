from GaudiKernel.SystemOfUnits import mm
from Configurables import FastVeloTracking
FastVeloTracking("FastVeloTracking").ZVertexMin = -2000.0*mm
FastVeloTracking("FastVeloTracking").ZVertexMax =  2000.0*mm
