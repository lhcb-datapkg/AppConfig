##############################################################################
# File for running Brunel on MC data with default Upgrade settings,
# and saving all MC Truth
#
# Syntax is:
# gaudirun.py Brunel/Upgrade-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Brunel

Brunel().DataType  = "Upgrade"
Brunel().InputType = "DIGI" # implies also Brunel().Simulation = True
Brunel().WithMC    = True   # implies also Brunel().Simulation = True

##############################################################################
