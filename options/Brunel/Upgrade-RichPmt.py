

from Configurables import RichRecSysConf,  RichRecQCConf
rConf = RichRecSysConf("RichOfflineRec")

# No Aerogel
rConf.Radiators = [ "Rich1Gas", "Rich2Gas" ]

# no photon sel cuts (need optimising)
rConf.photonConfig().SelectionMode = "All"

# Corrections to CK theta
rConf.richTools().photonReco().CKThetaQuartzRefractCorrections = [ -0.00625,  0.00010,  2.9e-5 ]


rMoni = RichRecQCConf("OfflineRichMoni")
rMoni.PidTrackTypes["Expert"] = [ ["Forward", "Match"] ]
rMoni.Radiators = rConf.Radiators
rMoni.removeMonitors(["L1SizeMonitoring", 
                      "DBConsistencyCheck",
                      "DataDecodingErrors",
                      "ODIN",
                      "HotPixelFinder",
                      "AlignmentMonitoring",
                      "HPDIFBMonitoring",
                      "HPDImageShifts",
                      "HPDHitPlots",
                      "RichPixelPositions",
                      "RichRecoTiming",
                      "RichTrackResolution",
                      "RichTrackCKResolutions",
                      "RichPhotonGeometry","PhotonRecoEfficiency",
                      "RichPhotonTrajectory","RichStereoFitterTests"]) # DB Problems
