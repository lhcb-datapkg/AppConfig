from Gaudi.Configuration import *
from Configurables import Brunel
Brunel().Hlt2FilterCode = "HLT_PASS_RE('^Hlt2(?!B[BE].*PassThrough)(?!BeamGasPassThrough)(?!Lumi).*Decision$') | HLT_PASS('Hlt2BESelectVeloMultPassThroughDecision')"    
