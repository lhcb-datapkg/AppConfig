# Modify behavior of Velo reconstruction i.e.
# splash events will not be rejected, so that for PbPb
# also event up to 20k velo clusters will be reconstructed
#
from Configurables import FastVeloTracking
FastVeloTracking("FastVeloTracking").MaxTrackClusterFrac=1.
