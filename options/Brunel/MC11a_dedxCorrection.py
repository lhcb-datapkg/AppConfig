# Apply a dEdx correction to the track fit, to make simulation pulls
# consistent with real data.
#
# N.B. This file does not work with new CloneKiller introduced in Brunel v42r3
# As from Brunel v42r3p1, this correction is done inside RecoTracking.py

from Configurables import TrackEventFitter, TrackMasterFitter
from Configurables import DetailedMaterialLocator
from Configurables import StateDetailedBetheBlochEnergyCorrectionTool
from Gaudi.Configuration import appendPostConfigAction

def applyTunedEnergyLossCorrection():
    FitBest = TrackEventFitter("FitBest")
    FitBest.addTool(TrackMasterFitter("Fitter"))
    FitBest.Fitter.addTool(DetailedMaterialLocator(), name="MaterialLocator")
    FitBest.Fitter.MaterialLocator.addTool(StateDetailedBetheBlochEnergyCorrectionTool("GeneralDedxTool"))
    FitBest.Fitter.MaterialLocator.GeneralDedxTool.EnergyLossFactor = 0.86

appendPostConfigAction(applyTunedEnergyLossCorrection)
