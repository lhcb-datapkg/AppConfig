##############################################################################
# File for running Brunel on MC data in stripping workflow,
# starting from strip ETC. DataType should be set separately
#
# Syntax is:
# gaudirun.py Brunel/MC-Stripping.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp

Brunel().InputType = "ETC"

from Configurables import  TagCollectionSvc
ApplicationMgr().ExtSvc  += [ TagCollectionSvc("EvtTupleSvc") ]

###############################################################################
# Example of input ETC file definition to be provided in <someDataFiles>.py
###############################################################################
#EventSelector().Input = [ "COLLECTION='TagCreator/EventTuple' DATAFILE='"SomeETC.root"' TYP='POOL_ROOT' SEL='(StrippingGlobal==1)'" ]
