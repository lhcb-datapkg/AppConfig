#repack certain raw banks for the stripping
from Gaudi.Configuration import GaudiSequencer, OutputStream
               
#first the Trigger Raw Event
from Configurables import RawEventSelectiveCopy
trigRawBankCopy = RawEventSelectiveCopy('TriggerRawBank')
trigRawBankCopy.RawBanksToCopy =[ 'ODIN',
                                  'HltSelReports' ,
                                  'HltDecReports',
                                  'L0Calo',
                                  'L0CaloFull',
                                  'L0DU',
                                  'L0Muon',
                                  'L0MuonProcCand',
                                  'L0PU'
                                ]
trigRawBankCopy.OutputRawEventLocation = "Trigger/RawEvent"
GaudiSequencer("OutputDSTSeq").Members +=[trigRawBankCopy]
               
#then the Muon Raw Event
muonRawBankCopy = RawEventSelectiveCopy('MuonRawBank')
muonRawBankCopy.RawBanksToCopy =[ 'Muon' ]
muonRawBankCopy.OutputRawEventLocation = "Muon/RawEvent"
GaudiSequencer("OutputDSTSeq").Members +=[muonRawBankCopy]

# add to the DST writer
writer = OutputStream( "DstWriter" )
writer.OptItemList += [
   "/Event/Trigger/RawEvent#1",
   "/Event/Muon/RawEvent#1"
]
