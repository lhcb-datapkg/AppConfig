# These options will run the special primary vertex algorithm for signal
# particle gun productions

from Configurables import Brunel
Brunel().SpecialData = [ 'pGun' ]
