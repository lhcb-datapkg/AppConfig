## ############################################################################
## # File for running Brunel with Upgrade database
## #
## # Upgrade Detectors: VL_Classic UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Brunel-Upgrade-VL_Classic-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
CondDB().AllLocalTagsByDataType = ["VL_Classic+UT", "FT"]

from Configurables import Brunel
Brunel().DataType     = "Upgrade" 

