from Gaudi.Configuration       import *
from Configurables             import Brunel
from GaudiKernel.SystemOfUnits import mm

from Configurables import TrackSys
TrackSys().OldCloneKiller = True 

from Configurables import TrackPrepareVelo, TrackBuildCloneTable
from Configurables import TrackCloneCleaner

TrackPrepareVelo().bestLocation = ""

cloneTable                = TrackBuildCloneTable('PreparedVeloFindTrackClones')
cloneTable.maxDz          = 500*mm                    # 2*mm default
cloneTable.zStates        = [ 0*mm, 990*mm, 9450*mm ] # [ 1, 0.0 ] default
cloneTable.klCut          = 5e3                       # 5000 default
cloneTable.inputLocation  = 'Rec/Track/PreparedVelo'
cloneTable.outputLocation = cloneTable.inputLocation + 'Clones'

cloneCleaner                = TrackCloneCleaner('PreparedVeloFlagTrackClones')
cloneCleaner.CloneCut       = 5e3 # 5000 default
cloneCleaner.inputLocation  = cloneTable.inputLocation
cloneCleaner.linkerLocation = cloneCleaner.inputLocation + 'Clones'

GaudiSequencer("TrackClonesSeq").Members  += [ cloneTable, cloneCleaner ]

OutputStream("DstWriter").ItemList  += ["/Event/Rec/Track/PreparedVelo#999"]

inputType = Brunel().getProp("InputType").upper()

if inputType == 'DIGI':
     OutputStream("DstWriter").ItemList += ["/Event/Link/Rec/Track/Velo#999"]
     OutputStream("DstWriter").ItemList += ["/Event/Link/Rec/Track/VeloInv#999"]

from Configurables import PatPVOffline, PVOfflineTool
from PatPV         import PVConf

# LOOSE PVS FROM BEST TRACKS
LoosePV = PatPVOffline('LoosePV')
PVConf.LoosePV().configureAlg(LoosePV)
LoosePV.PVOfflineTool.PVSeedingName = 'PVSeedTool'
LoosePV.OutputVertices              = 'Rec/Vertex/Loose/Primary'

# STD PVS FROM PREPARED VELO TRACKS	
preparedVeloPV                             = PatPVOffline('preparedVeloPV')
preparedVeloPV.addTool(PVOfflineTool, 'PVOfflineTool')
preparedVeloPV.PVOfflineTool.PVSeedingName = 'PVSeedTool'
preparedVeloPV.PVOfflineTool.InputTracks   = ['Rec/Track/PreparedVelo']
preparedVeloPV.OutputVertices              = 'Rec/Vertex/PreparedVelo/Primary'

# LOOSE PVS FROM PREPARED VELO TRACKS	
preparedVeloLoosePV = PatPVOffline('preparedVeloLoosePV')
PVConf.LoosePV().configureAlg(preparedVeloLoosePV)
preparedVeloLoosePV.PVOfflineTool.PVSeedingName = 'PVSeedTool'
preparedVeloLoosePV.PVOfflineTool.InputTracks   = ['Rec/Track/PreparedVelo']
preparedVeloLoosePV.OutputVertices              = 'Rec/Vertex/PreparedVelo/Loose/Primary'

GaudiSequencer("RecoVertexSeq").Members  += [ LoosePV, preparedVeloLoosePV ]

OutputStream("DstWriter").ItemList  += ['Rec/Vertex/Loose/Primary' + "#1"]

OutputStream("DstWriter").ItemList  += ['Rec/Vertex/PreparedVelo/Loose/Primary' + "#1"]

