## ############################################################################
## # File for running Boole with Upgrade database
## #
## # Upgrade Detectors: VP UT FT Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Boole-Upgrade-VP-UT-FT.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *

from Configurables import Boole
Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'MC']


