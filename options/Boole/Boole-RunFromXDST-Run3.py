from Boole.Configuration import *

boole = Boole()
# kill xdst TES locations added by Moore
xdstPaths = [
    #added by Moore:
    # "/Event/Trigger",
    "/Event/HLT2",
    "/Event/Rich",
    "/Event/Calo",
    "/Event/Muon",
    "/Event/Other",
    #added by Boole
    "/Event/Link/Raw",
    "/Event/DAQ",
    "/Event/pSim/Rich/DigitSummaries",
    "/Event/MC/TrackInfo",
    "/Event/MC/Muon",
    "/Event/MC/DigiHeader",
    #not always there, depends on the DataType and format
    "/Event/Link/Trig",
    "/Event/MC/Rich/DigitSummaries",
    "/Event/Prev/DAQ",
    "/Event/PrevPrev/DAQ",
    "/Event/Next/DAQ",
    "/Event/NextNext/DAQ"
    '/Event/MC/DigiHeader',
    '/Event/MC/Muon',
    '/Event/MC/Muon/DigitsInfo',
    '/Event/MC/TrackInfo',
    '/Event/Link/Raw/VP/Digits2MCHits',
    '/Event/Link/Raw/VP/Digits',
    '/Event/Link/Raw/VP',
    '/Event/Link/Raw/UT/Clusters2MCHits',
    '/Event/Link/Raw/UT/Clusters',
    '/Event/Link/Raw/UT',
    '/Event/Link/Raw/FT/LiteClusters',
    '/Event/Link/Raw/FT/LiteClustersWithSpillover',
    '/Event/Link/Raw/FT/LiteClusters2MCHits',
    '/Event/Link/Raw/FT/LiteClusters2MCHitsWithSpillover',
    '/Event/Link/Raw/FT',
    '/Event/Link/Raw/Ecal/Digits2MCParticles',
    '/Event/Link/Raw/Ecal',
    '/Event/Link/Raw/Hcal/Digits2MCParticles',
    '/Event/Link/Raw/Hcal',
    '/Event/Link/Raw/Muon/Digits',
    '/Event/Link/Raw/Muon',
    '/Event/Link/Raw',
    '/Event/pSim/Rich/DigitSummaries',
    '/Event/Rec/Header',
    '/Event/Rec/Status',
    '/Event/Rec/Summary',
    '/Event/Trigger/RawEvent',
    '/Event/Tracker/RawEvent',
    '/Event/Velo/RawEvent',
    '/Event/Calo/RawEvent',
    '/Event/Calo',
    '/Event/Muon/RawEvent',
    '/Event/Muon',
    '/Event/Rich/RawEvent',
    '/Event/Rich',
    '/Event/Other/RawEvent',
    '/Event/Other'
]

extraLoad = [
    '/Event/Link/MC/Rich/Hits2MCRichOpticalPhotons',
    '/Event/Link/MC/Particles2MCRichTracks'
]


from Configurables import (TESCheck, EventNodeKiller)
initBoole = GaudiSequencer("InitBooleSeq")
xdstLoader = TESCheck("XDSTLoader")
xdstLoader.Inputs = xdstPaths + extraLoad
xdstLoader.Stop = False  # If not MC do not expect all of the entries
xdstLoader.OutputLevel = ERROR
xdstKiller = EventNodeKiller("XDSTKiller")
xdstKiller.Nodes = xdstPaths
xdstHandler = GaudiSequencer("XDSTLoverHandler")
xdstHandler.Members += [xdstLoader, xdstKiller]
xdstHandler.IgnoreFilterPassed = True  # keep going
initBoole.Members += [xdstHandler]

from GaudiConf import IOHelper


def patch():
    OutputStream("DigiWriter").ItemList += ["/Event/Link/MC#1"]

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(patch)
