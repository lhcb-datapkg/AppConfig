##############################################################################
# File for running Boole with Upgrade configuration
##############################################################################

from Boole.Configuration import *


from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True

if "Calo_NoSPDPRS" not in CondDB().AllLocalTagsByDataType:
    CondDB().AllLocalTagsByDataType += ["Calo_NoSPDPRS"]
 
from Configurables import Boole
for det in ["Spd", "Prs"]:
    if det in Boole().DetectorDigi:
        Boole().DetectorDigi.remove(det)

for det in ["Spd", "Prs"]:
    if det in Boole().DetectorLink:
        Boole().DetectorLink.remove(det)

for det in ["Spd", "Prs"]:
    if det in Boole().DetectorMoni:
        Boole().DetectorMoni.remove(det)

Boole().DataType = "Upgrade"

