## Options to switch off all geometry (and related simulation)
## save that of calorimeters area using Boole v26r2 and higher
##
## Author: P.Szczypka, G.Corti
## Date:   2012-03-07
##
from Gaudi.Configuration import *

from Configurables import Boole
Boole().DetectorDigi = ['Spd', 'Prs', 'Ecal', 'Hcal']
Boole().DetectorLink = ['Spd', 'Prs', 'Ecal', 'Hcal']
Boole().DetectorMoni = ['Spd', 'Prs', 'Ecal', 'Hcal', 'MC']
