## ############################################################################
## # File for running Boole with Upgrade database
## #
## # Upgrade Detectors: Rich1Pmt Rich2Pmt
## #
## # Syntax is:
## #   gaudirun.py Boole-Upgrade-Reference.py <someInputJobConfiguration>.py
## ############################################################################

from Gaudi.Configuration import *
from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Boole
Boole().DetectorDigi = ['Velo', 'TT', 'IT', 'OT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon']
Boole().DetectorLink = ['Velo', 'TT', 'IT', 'OT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Tr']
Boole().DetectorMoni = ['Velo', 'TT', 'IT', 'OT', 'Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'MC']

