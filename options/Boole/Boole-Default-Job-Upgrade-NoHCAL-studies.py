# $Id: Upgrade-WithTruth.py,v 1.2 2009-10-02 10:32:13 tskwarni Exp $
##############################################################################
# File for running Boole with Upgrade configuration, with full Digi output
#
# Syntax is:
# gaudirun.py Boole/Upgrade-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# just instantiate the configurable
theApp = Boole()
# set special data type for upgrade simulations
theApp.DataType  = "Upgrade"
# enable spillover
theApp.UseSpillover = True
#   
from Configurables import DigiConf
DigiConf().SpilloverPaths = ["Prev", "PrevPrev", "Next"]
##############################################################################

Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon']
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Tr']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Muon', 'Tr', 'MC']

from Configurables import CondDB
CondDB().Upgrade     = True

from Configurables import Boole
#--Set database tags using those for Sim08

for det in ["Ecal","Hcal"]:
    if det in Boole().DetectorDigi:
        Boole().DetectorDigi.remove(det)

for det in ["Ecal","Hcal"]:
    if det in Boole().DetectorLink:
        Boole().DetectorLink.remove(det)

for det in ["Ecal","Hcal"]:
    if det in Boole().DetectorMoni:
        Boole().DetectorMoni.remove(det)

