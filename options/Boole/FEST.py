##############################################################################
# File for Boole production of MDF files for Full Experiment System Test
#
# Syntax is:
# gaudirun.py Boole/FEST.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

Boole().Outputs    = ["MDF"]

# Do not compress output file, for faster reading by FEST injector
from Configurables import LHCb__RawDataCnvSvc
LHCb__RawDataCnvSvc().Compress = 0
