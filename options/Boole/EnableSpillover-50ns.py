# enable spillover 
from Configurables import Boole
Boole().UseSpillover = True
#   
from Configurables import DigiConf
#DigiConf().SpilloverPaths = ["PrevPrev", "NextNext"]
Boole().SpilloverPaths = ["PrevPrev", "NextNext"]

# configure OT deposit creator to use PrevPrev and NextNext
from GaudiKernel.SystemOfUnits import ns
from Configurables import MCOTDepositCreator
MCOTDepositCreator().SpillVector = [ "/PrevPrev/",
                                     "/",
                                     "/NextNext/" ]
MCOTDepositCreator().SpillTimes  = [ -50.0 * ns,
                                       0.0 * ns,
                                      50.0 * ns ]
