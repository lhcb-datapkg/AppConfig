############################################################################
# File for running Boole with only VP. Designed for Run 3.
############################################################################

from Configurables import Boole

Boole().DetectorDigi = ['VP']
Boole().DetectorLink = ['VP', 'Tr']
Boole().DetectorMoni = ['VP', 'MC']

from Configurables import BuildMCTrackInfo
BuildMCTrackInfo().WithFT = False

