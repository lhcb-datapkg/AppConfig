##############################################################################
# File for running Boole with Minimal Digi output
#
# Syntax is:
# gaudirun.py Boole/NoTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

# Switch off MC truth output except for primary vertices information
Boole().DigiType = 'Minimal'

##############################################################################
